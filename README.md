# README #

BoxPress

### What is this repository for? ###

* Imagebox WordPress Framework
* Version 3.0.0

### How do I get set up? ###

* Install Theme
* Download/Install Advanced Custom Fields
* Go to Advanced Custom Fields 'Field Groups' page, click on 'Sync' link and then Sync all fields.
* Navigate to the boxpress folder in terminal and run 'npm install'

### Contribution guidelines ###

* Talk to Brad

### Who do I talk to? ###

* JD's mom


### Docker

#### Running Local Docker Build

To get the Docker build up and running, make sure you have Docker installed:
https://www.docker.com/community-edition

Once installed, cd to your project in Terminal, and run the following command:

  docker-compose up

This will download and spin up a local server.


#### Managing Plugins

Any plugins you install will also need to be added to the docker-compose.yml file. Under the "Plugins" option, add the slug of the plugin you need.

The first time docker-compose runs, it'll empty out the plugins directory and install what's listed.


#### Export Site Files

To export your site, make sure docker-compose is running, and use the `docker export` command. First you'll need the container name.

To get the name of the container, run:

  docker ps

The name will be something like "wpproject_wordpress_1". Then run:

  docker export --output="backup.tar" wpproject_wordpress_1

The export will contain the entire server. Your Wordpress site is under '/app/'.

Also note any directorys synced with Docker such as the theme folder won't be included.


#### Export Site DB

To export the site DB, make sure docker-compose is running, and to go phpMyAdmin at:
http://localhost:22222/

The site DB is named "wordpress". Using the custom export method, set the compatability to "MYSQL40" under "Format-specific Options".
