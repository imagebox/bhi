(function ($) {
  'use strict';

  /**
   * Navigation Search Toggle
   */
  
  var $nav_search_toggle    = $('.js-search-toggle');
  var $nav_search_submit  = $('.nav-search-dropdown .search-submit');

  // Search toggle button
  $nav_search_toggle.on('click', function () {
    var $this = $(this);
    var $this_parent    = $this.parent( '.nav-item-search' );
    var $this_dropdown  = $this.next( '.nav-search-dropdown' );

    // Track show / hide state on parent
    $this_parent.toggleClass('is-active');

    if ( $this_parent.hasClass( 'is-active' )) {
      // Show Dropdown
      show_search_dropdown( $this, $this_dropdown );

    } else {
      // Hide Dropdown
      hide_search_dropdown( $this, $this_dropdown );
    }

    return false;
  });


  /**
   * Hide on 'esc' keypress
   */

  $(document).keydown( function (e) {
    if ( e.keyCode === 27 && $nav_search_toggle.length ) {
      var $button_parent    = $nav_search_toggle.parent( '.nav-item-search' );
      var $button_dropdown  = $nav_search_toggle.next( '.nav-search-dropdown' );

      hide_search_dropdown( $nav_search_toggle, $button_dropdown );
      $button_parent.removeClass( 'is-active' );
    }
  });


  /**
   * Hide on tab out
   */
  
  $nav_search_submit.on('keydown', function ( e ) {
    if ( e.which === 9 && ! e.shiftKey ) {
      var $button_parent    = $nav_search_toggle.parent( '.nav-item-search' );
      var $button_dropdown  = $nav_search_toggle.next( '.nav-search-dropdown' );

      hide_search_dropdown( $nav_search_toggle, $button_dropdown );
      $button_parent.removeClass( 'is-active' );
    }
  });

  $nav_search_toggle.on('keydown', function ( e ) {
    if ( e.which === 9 && e.shiftKey ) {
      var $this = $(this);
      var $button_parent    = $this.parent( '.nav-item-search' );
      var $button_dropdown  = $this.next( '.nav-search-dropdown' );

      hide_search_dropdown( $nav_search_toggle, $button_dropdown );
      $button_parent.removeClass( 'is-active' );
    }
  });


  /**
   * Show Search Dropdown
   */
  function show_search_dropdown( $toggle_button, $dropdown ) {
    $toggle_button.attr( 'aria-expanded', 'true' );
    $dropdown.css( 'display', 'block' );
    $dropdown.find( 'input[type="search"]' ).focus();
  }


  /**
   * Hide Search Dropdown
   */
  function hide_search_dropdown( $toggle_button, $dropdown ) {
    $toggle_button.attr( 'aria-expanded', 'false' );
    $dropdown.css( 'display', 'none' );
  }

})(jQuery);
