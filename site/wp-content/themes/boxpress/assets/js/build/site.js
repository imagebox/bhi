!function(a){"function"==typeof define&&define.amd?define(["jquery"],a):a("object"==typeof exports?require("jquery"):window.jQuery||window.Zepto)}(function(a){var b,c,d,e,f,g,h="Close",i="BeforeClose",j="AfterClose",k="BeforeAppend",l="MarkupParse",m="Open",n="Change",o="mfp",p="."+o,q="mfp-ready",r="mfp-removing",s="mfp-prevent-close",t=function(){},u=!!window.jQuery,v=a(window),w=function(a,c){b.ev.on(o+a+p,c)},x=function(b,c,d,e){var f=document.createElement("div");return f.className="mfp-"+b,d&&(f.innerHTML=d),e?c&&c.appendChild(f):(f=a(f),c&&f.appendTo(c)),f},y=function(c,d){b.ev.triggerHandler(o+c,d),b.st.callbacks&&(c=c.charAt(0).toLowerCase()+c.slice(1),b.st.callbacks[c]&&b.st.callbacks[c].apply(b,a.isArray(d)?d:[d]))},z=function(c){return c===g&&b.currTemplate.closeBtn||(b.currTemplate.closeBtn=a(b.st.closeMarkup.replace("%title%",b.st.tClose)),g=c),b.currTemplate.closeBtn},A=function(){a.magnificPopup.instance||(b=new t,b.init(),a.magnificPopup.instance=b)},B=function(){var a=document.createElement("p").style,b=["ms","O","Moz","Webkit"];if(void 0!==a.transition)return!0;for(;b.length;)if(b.pop()+"Transition"in a)return!0;return!1};t.prototype={constructor:t,init:function(){var c=navigator.appVersion;b.isLowIE=b.isIE8=document.all&&!document.addEventListener,b.isAndroid=/android/gi.test(c),b.isIOS=/iphone|ipad|ipod/gi.test(c),b.supportsTransition=B(),b.probablyMobile=b.isAndroid||b.isIOS||/(Opera Mini)|Kindle|webOS|BlackBerry|(Opera Mobi)|(Windows Phone)|IEMobile/i.test(navigator.userAgent),d=a(document),b.popupsCache={}},open:function(c){var e;if(c.isObj===!1){b.items=c.items.toArray(),b.index=0;var g,h=c.items;for(e=0;e<h.length;e++)if(g=h[e],g.parsed&&(g=g.el[0]),g===c.el[0]){b.index=e;break}}else b.items=a.isArray(c.items)?c.items:[c.items],b.index=c.index||0;if(b.isOpen)return void b.updateItemHTML();b.types=[],f="",c.mainEl&&c.mainEl.length?b.ev=c.mainEl.eq(0):b.ev=d,c.key?(b.popupsCache[c.key]||(b.popupsCache[c.key]={}),b.currTemplate=b.popupsCache[c.key]):b.currTemplate={},b.st=a.extend(!0,{},a.magnificPopup.defaults,c),b.fixedContentPos="auto"===b.st.fixedContentPos?!b.probablyMobile:b.st.fixedContentPos,b.st.modal&&(b.st.closeOnContentClick=!1,b.st.closeOnBgClick=!1,b.st.showCloseBtn=!1,b.st.enableEscapeKey=!1),b.bgOverlay||(b.bgOverlay=x("bg").on("click"+p,function(){b.close()}),b.wrap=x("wrap").attr("tabindex",-1).on("click"+p,function(a){b._checkIfClose(a.target)&&b.close()}),b.container=x("container",b.wrap)),b.contentContainer=x("content"),b.st.preloader&&(b.preloader=x("preloader",b.container,b.st.tLoading));var i=a.magnificPopup.modules;for(e=0;e<i.length;e++){var j=i[e];j=j.charAt(0).toUpperCase()+j.slice(1),b["init"+j].call(b)}y("BeforeOpen"),b.st.showCloseBtn&&(b.st.closeBtnInside?(w(l,function(a,b,c,d){c.close_replaceWith=z(d.type)}),f+=" mfp-close-btn-in"):b.wrap.append(z())),b.st.alignTop&&(f+=" mfp-align-top"),b.fixedContentPos?b.wrap.css({overflow:b.st.overflowY,overflowX:"hidden",overflowY:b.st.overflowY}):b.wrap.css({top:v.scrollTop(),position:"absolute"}),(b.st.fixedBgPos===!1||"auto"===b.st.fixedBgPos&&!b.fixedContentPos)&&b.bgOverlay.css({height:d.height(),position:"absolute"}),b.st.enableEscapeKey&&d.on("keyup"+p,function(a){27===a.keyCode&&b.close()}),v.on("resize"+p,function(){b.updateSize()}),b.st.closeOnContentClick||(f+=" mfp-auto-cursor"),f&&b.wrap.addClass(f);var k=b.wH=v.height(),n={};if(b.fixedContentPos&&b._hasScrollBar(k)){var o=b._getScrollbarSize();o&&(n.marginRight=o)}b.fixedContentPos&&(b.isIE7?a("body, html").css("overflow","hidden"):n.overflow="hidden");var r=b.st.mainClass;return b.isIE7&&(r+=" mfp-ie7"),r&&b._addClassToMFP(r),b.updateItemHTML(),y("BuildControls"),a("html").css(n),b.bgOverlay.add(b.wrap).prependTo(b.st.prependTo||a(document.body)),b._lastFocusedEl=document.activeElement,setTimeout(function(){b.content?(b._addClassToMFP(q),b._setFocus()):b.bgOverlay.addClass(q),d.on("focusin"+p,b._onFocusIn)},16),b.isOpen=!0,b.updateSize(k),y(m),c},close:function(){b.isOpen&&(y(i),b.isOpen=!1,b.st.removalDelay&&!b.isLowIE&&b.supportsTransition?(b._addClassToMFP(r),setTimeout(function(){b._close()},b.st.removalDelay)):b._close())},_close:function(){y(h);var c=r+" "+q+" ";if(b.bgOverlay.detach(),b.wrap.detach(),b.container.empty(),b.st.mainClass&&(c+=b.st.mainClass+" "),b._removeClassFromMFP(c),b.fixedContentPos){var e={marginRight:""};b.isIE7?a("body, html").css("overflow",""):e.overflow="",a("html").css(e)}d.off("keyup"+p+" focusin"+p),b.ev.off(p),b.wrap.attr("class","mfp-wrap").removeAttr("style"),b.bgOverlay.attr("class","mfp-bg"),b.container.attr("class","mfp-container"),!b.st.showCloseBtn||b.st.closeBtnInside&&b.currTemplate[b.currItem.type]!==!0||b.currTemplate.closeBtn&&b.currTemplate.closeBtn.detach(),b.st.autoFocusLast&&b._lastFocusedEl&&a(b._lastFocusedEl).focus(),b.currItem=null,b.content=null,b.currTemplate=null,b.prevHeight=0,y(j)},updateSize:function(a){if(b.isIOS){var c=document.documentElement.clientWidth/window.innerWidth,d=window.innerHeight*c;b.wrap.css("height",d),b.wH=d}else b.wH=a||v.height();b.fixedContentPos||b.wrap.css("height",b.wH),y("Resize")},updateItemHTML:function(){var c=b.items[b.index];b.contentContainer.detach(),b.content&&b.content.detach(),c.parsed||(c=b.parseEl(b.index));var d=c.type;if(y("BeforeChange",[b.currItem?b.currItem.type:"",d]),b.currItem=c,!b.currTemplate[d]){var f=b.st[d]?b.st[d].markup:!1;y("FirstMarkupParse",f),f?b.currTemplate[d]=a(f):b.currTemplate[d]=!0}e&&e!==c.type&&b.container.removeClass("mfp-"+e+"-holder");var g=b["get"+d.charAt(0).toUpperCase()+d.slice(1)](c,b.currTemplate[d]);b.appendContent(g,d),c.preloaded=!0,y(n,c),e=c.type,b.container.prepend(b.contentContainer),y("AfterChange")},appendContent:function(a,c){b.content=a,a?b.st.showCloseBtn&&b.st.closeBtnInside&&b.currTemplate[c]===!0?b.content.find(".mfp-close").length||b.content.append(z()):b.content=a:b.content="",y(k),b.container.addClass("mfp-"+c+"-holder"),b.contentContainer.append(b.content)},parseEl:function(c){var d,e=b.items[c];if(e.tagName?e={el:a(e)}:(d=e.type,e={data:e,src:e.src}),e.el){for(var f=b.types,g=0;g<f.length;g++)if(e.el.hasClass("mfp-"+f[g])){d=f[g];break}e.src=e.el.attr("data-mfp-src"),e.src||(e.src=e.el.attr("href"))}return e.type=d||b.st.type||"inline",e.index=c,e.parsed=!0,b.items[c]=e,y("ElementParse",e),b.items[c]},addGroup:function(a,c){var d=function(d){d.mfpEl=this,b._openClick(d,a,c)};c||(c={});var e="click.magnificPopup";c.mainEl=a,c.items?(c.isObj=!0,a.off(e).on(e,d)):(c.isObj=!1,c.delegate?a.off(e).on(e,c.delegate,d):(c.items=a,a.off(e).on(e,d)))},_openClick:function(c,d,e){var f=void 0!==e.midClick?e.midClick:a.magnificPopup.defaults.midClick;if(f||!(2===c.which||c.ctrlKey||c.metaKey||c.altKey||c.shiftKey)){var g=void 0!==e.disableOn?e.disableOn:a.magnificPopup.defaults.disableOn;if(g)if(a.isFunction(g)){if(!g.call(b))return!0}else if(v.width()<g)return!0;c.type&&(c.preventDefault(),b.isOpen&&c.stopPropagation()),e.el=a(c.mfpEl),e.delegate&&(e.items=d.find(e.delegate)),b.open(e)}},updateStatus:function(a,d){if(b.preloader){c!==a&&b.container.removeClass("mfp-s-"+c),d||"loading"!==a||(d=b.st.tLoading);var e={status:a,text:d};y("UpdateStatus",e),a=e.status,d=e.text,b.preloader.html(d),b.preloader.find("a").on("click",function(a){a.stopImmediatePropagation()}),b.container.addClass("mfp-s-"+a),c=a}},_checkIfClose:function(c){if(!a(c).hasClass(s)){var d=b.st.closeOnContentClick,e=b.st.closeOnBgClick;if(d&&e)return!0;if(!b.content||a(c).hasClass("mfp-close")||b.preloader&&c===b.preloader[0])return!0;if(c===b.content[0]||a.contains(b.content[0],c)){if(d)return!0}else if(e&&a.contains(document,c))return!0;return!1}},_addClassToMFP:function(a){b.bgOverlay.addClass(a),b.wrap.addClass(a)},_removeClassFromMFP:function(a){this.bgOverlay.removeClass(a),b.wrap.removeClass(a)},_hasScrollBar:function(a){return(b.isIE7?d.height():document.body.scrollHeight)>(a||v.height())},_setFocus:function(){(b.st.focus?b.content.find(b.st.focus).eq(0):b.wrap).focus()},_onFocusIn:function(c){return c.target===b.wrap[0]||a.contains(b.wrap[0],c.target)?void 0:(b._setFocus(),!1)},_parseMarkup:function(b,c,d){var e;d.data&&(c=a.extend(d.data,c)),y(l,[b,c,d]),a.each(c,function(c,d){if(void 0===d||d===!1)return!0;if(e=c.split("_"),e.length>1){var f=b.find(p+"-"+e[0]);if(f.length>0){var g=e[1];"replaceWith"===g?f[0]!==d[0]&&f.replaceWith(d):"img"===g?f.is("img")?f.attr("src",d):f.replaceWith(a("<img>").attr("src",d).attr("class",f.attr("class"))):f.attr(e[1],d)}}else b.find(p+"-"+c).html(d)})},_getScrollbarSize:function(){if(void 0===b.scrollbarSize){var a=document.createElement("div");a.style.cssText="width: 99px; height: 99px; overflow: scroll; position: absolute; top: -9999px;",document.body.appendChild(a),b.scrollbarSize=a.offsetWidth-a.clientWidth,document.body.removeChild(a)}return b.scrollbarSize}},a.magnificPopup={instance:null,proto:t.prototype,modules:[],open:function(b,c){return A(),b=b?a.extend(!0,{},b):{},b.isObj=!0,b.index=c||0,this.instance.open(b)},close:function(){return a.magnificPopup.instance&&a.magnificPopup.instance.close()},registerModule:function(b,c){c.options&&(a.magnificPopup.defaults[b]=c.options),a.extend(this.proto,c.proto),this.modules.push(b)},defaults:{disableOn:0,key:null,midClick:!1,mainClass:"",preloader:!0,focus:"",closeOnContentClick:!1,closeOnBgClick:!0,closeBtnInside:!0,showCloseBtn:!0,enableEscapeKey:!0,modal:!1,alignTop:!1,removalDelay:0,prependTo:null,fixedContentPos:"auto",fixedBgPos:"auto",overflowY:"auto",closeMarkup:'<button title="%title%" type="button" class="mfp-close">&#215;</button>',tClose:"Close (Esc)",tLoading:"Loading...",autoFocusLast:!0}},a.fn.magnificPopup=function(c){A();var d=a(this);if("string"==typeof c)if("open"===c){var e,f=u?d.data("magnificPopup"):d[0].magnificPopup,g=parseInt(arguments[1],10)||0;f.items?e=f.items[g]:(e=d,f.delegate&&(e=e.find(f.delegate)),e=e.eq(g)),b._openClick({mfpEl:e},d,f)}else b.isOpen&&b[c].apply(b,Array.prototype.slice.call(arguments,1));else c=a.extend(!0,{},c),u?d.data("magnificPopup",c):d[0].magnificPopup=c,b.addGroup(d,c);return d};var C,D,E,F="inline",G=function(){E&&(D.after(E.addClass(C)).detach(),E=null)};a.magnificPopup.registerModule(F,{options:{hiddenClass:"hide",markup:"",tNotFound:"Content not found"},proto:{initInline:function(){b.types.push(F),w(h+"."+F,function(){G()})},getInline:function(c,d){if(G(),c.src){var e=b.st.inline,f=a(c.src);if(f.length){var g=f[0].parentNode;g&&g.tagName&&(D||(C=e.hiddenClass,D=x(C),C="mfp-"+C),E=f.after(D).detach().removeClass(C)),b.updateStatus("ready")}else b.updateStatus("error",e.tNotFound),f=a("<div>");return c.inlineElement=f,f}return b.updateStatus("ready"),b._parseMarkup(d,{},c),d}}});var H,I="ajax",J=function(){H&&a(document.body).removeClass(H)},K=function(){J(),b.req&&b.req.abort()};a.magnificPopup.registerModule(I,{options:{settings:null,cursor:"mfp-ajax-cur",tError:'<a href="%url%">The content</a> could not be loaded.'},proto:{initAjax:function(){b.types.push(I),H=b.st.ajax.cursor,w(h+"."+I,K),w("BeforeChange."+I,K)},getAjax:function(c){H&&a(document.body).addClass(H),b.updateStatus("loading");var d=a.extend({url:c.src,success:function(d,e,f){var g={data:d,xhr:f};y("ParseAjax",g),b.appendContent(a(g.data),I),c.finished=!0,J(),b._setFocus(),setTimeout(function(){b.wrap.addClass(q)},16),b.updateStatus("ready"),y("AjaxContentAdded")},error:function(){J(),c.finished=c.loadError=!0,b.updateStatus("error",b.st.ajax.tError.replace("%url%",c.src))}},b.st.ajax.settings);return b.req=a.ajax(d),""}}});var L,M=function(c){if(c.data&&void 0!==c.data.title)return c.data.title;var d=b.st.image.titleSrc;if(d){if(a.isFunction(d))return d.call(b,c);if(c.el)return c.el.attr(d)||""}return""};a.magnificPopup.registerModule("image",{options:{markup:'<div class="mfp-figure"><div class="mfp-close"></div><figure><div class="mfp-img"></div><figcaption><div class="mfp-bottom-bar"><div class="mfp-title"></div><div class="mfp-counter"></div></div></figcaption></figure></div>',cursor:"mfp-zoom-out-cur",titleSrc:"title",verticalFit:!0,tError:'<a href="%url%">The image</a> could not be loaded.'},proto:{initImage:function(){var c=b.st.image,d=".image";b.types.push("image"),w(m+d,function(){"image"===b.currItem.type&&c.cursor&&a(document.body).addClass(c.cursor)}),w(h+d,function(){c.cursor&&a(document.body).removeClass(c.cursor),v.off("resize"+p)}),w("Resize"+d,b.resizeImage),b.isLowIE&&w("AfterChange",b.resizeImage)},resizeImage:function(){var a=b.currItem;if(a&&a.img&&b.st.image.verticalFit){var c=0;b.isLowIE&&(c=parseInt(a.img.css("padding-top"),10)+parseInt(a.img.css("padding-bottom"),10)),a.img.css("max-height",b.wH-c)}},_onImageHasSize:function(a){a.img&&(a.hasSize=!0,L&&clearInterval(L),a.isCheckingImgSize=!1,y("ImageHasSize",a),a.imgHidden&&(b.content&&b.content.removeClass("mfp-loading"),a.imgHidden=!1))},findImageSize:function(a){var c=0,d=a.img[0],e=function(f){L&&clearInterval(L),L=setInterval(function(){return d.naturalWidth>0?void b._onImageHasSize(a):(c>200&&clearInterval(L),c++,void(3===c?e(10):40===c?e(50):100===c&&e(500)))},f)};e(1)},getImage:function(c,d){var e=0,f=function(){c&&(c.img[0].complete?(c.img.off(".mfploader"),c===b.currItem&&(b._onImageHasSize(c),b.updateStatus("ready")),c.hasSize=!0,c.loaded=!0,y("ImageLoadComplete")):(e++,200>e?setTimeout(f,100):g()))},g=function(){c&&(c.img.off(".mfploader"),c===b.currItem&&(b._onImageHasSize(c),b.updateStatus("error",h.tError.replace("%url%",c.src))),c.hasSize=!0,c.loaded=!0,c.loadError=!0)},h=b.st.image,i=d.find(".mfp-img");if(i.length){var j=document.createElement("img");j.className="mfp-img",c.el&&c.el.find("img").length&&(j.alt=c.el.find("img").attr("alt")),c.img=a(j).on("load.mfploader",f).on("error.mfploader",g),j.src=c.src,i.is("img")&&(c.img=c.img.clone()),j=c.img[0],j.naturalWidth>0?c.hasSize=!0:j.width||(c.hasSize=!1)}return b._parseMarkup(d,{title:M(c),img_replaceWith:c.img},c),b.resizeImage(),c.hasSize?(L&&clearInterval(L),c.loadError?(d.addClass("mfp-loading"),b.updateStatus("error",h.tError.replace("%url%",c.src))):(d.removeClass("mfp-loading"),b.updateStatus("ready")),d):(b.updateStatus("loading"),c.loading=!0,c.hasSize||(c.imgHidden=!0,d.addClass("mfp-loading"),b.findImageSize(c)),d)}}});var N,O=function(){return void 0===N&&(N=void 0!==document.createElement("p").style.MozTransform),N};a.magnificPopup.registerModule("zoom",{options:{enabled:!1,easing:"ease-in-out",duration:300,opener:function(a){return a.is("img")?a:a.find("img")}},proto:{initZoom:function(){var a,c=b.st.zoom,d=".zoom";if(c.enabled&&b.supportsTransition){var e,f,g=c.duration,j=function(a){var b=a.clone().removeAttr("style").removeAttr("class").addClass("mfp-animated-image"),d="all "+c.duration/1e3+"s "+c.easing,e={position:"fixed",zIndex:9999,left:0,top:0,"-webkit-backface-visibility":"hidden"},f="transition";return e["-webkit-"+f]=e["-moz-"+f]=e["-o-"+f]=e[f]=d,b.css(e),b},k=function(){b.content.css("visibility","visible")};w("BuildControls"+d,function(){if(b._allowZoom()){if(clearTimeout(e),b.content.css("visibility","hidden"),a=b._getItemToZoom(),!a)return void k();f=j(a),f.css(b._getOffset()),b.wrap.append(f),e=setTimeout(function(){f.css(b._getOffset(!0)),e=setTimeout(function(){k(),setTimeout(function(){f.remove(),a=f=null,y("ZoomAnimationEnded")},16)},g)},16)}}),w(i+d,function(){if(b._allowZoom()){if(clearTimeout(e),b.st.removalDelay=g,!a){if(a=b._getItemToZoom(),!a)return;f=j(a)}f.css(b._getOffset(!0)),b.wrap.append(f),b.content.css("visibility","hidden"),setTimeout(function(){f.css(b._getOffset())},16)}}),w(h+d,function(){b._allowZoom()&&(k(),f&&f.remove(),a=null)})}},_allowZoom:function(){return"image"===b.currItem.type},_getItemToZoom:function(){return b.currItem.hasSize?b.currItem.img:!1},_getOffset:function(c){var d;d=c?b.currItem.img:b.st.zoom.opener(b.currItem.el||b.currItem);var e=d.offset(),f=parseInt(d.css("padding-top"),10),g=parseInt(d.css("padding-bottom"),10);e.top-=a(window).scrollTop()-f;var h={width:d.width(),height:(u?d.innerHeight():d[0].offsetHeight)-g-f};return O()?h["-moz-transform"]=h.transform="translate("+e.left+"px,"+e.top+"px)":(h.left=e.left,h.top=e.top),h}}});var P="iframe",Q="//about:blank",R=function(a){if(b.currTemplate[P]){var c=b.currTemplate[P].find("iframe");c.length&&(a||(c[0].src=Q),b.isIE8&&c.css("display",a?"block":"none"))}};a.magnificPopup.registerModule(P,{options:{markup:'<div class="mfp-iframe-scaler"><div class="mfp-close"></div><iframe class="mfp-iframe" src="//about:blank" frameborder="0" allowfullscreen></iframe></div>',srcAction:"iframe_src",patterns:{youtube:{index:"youtube.com",id:"v=",src:"//www.youtube.com/embed/%id%?autoplay=1"},vimeo:{index:"vimeo.com/",id:"/",src:"//player.vimeo.com/video/%id%?autoplay=1"},gmaps:{index:"//maps.google.",src:"%id%&output=embed"}}},proto:{initIframe:function(){b.types.push(P),w("BeforeChange",function(a,b,c){b!==c&&(b===P?R():c===P&&R(!0))}),w(h+"."+P,function(){R()})},getIframe:function(c,d){var e=c.src,f=b.st.iframe;a.each(f.patterns,function(){return e.indexOf(this.index)>-1?(this.id&&(e="string"==typeof this.id?e.substr(e.lastIndexOf(this.id)+this.id.length,e.length):this.id.call(this,e)),e=this.src.replace("%id%",e),!1):void 0});var g={};return f.srcAction&&(g[f.srcAction]=e),b._parseMarkup(d,g,c),b.updateStatus("ready"),d}}});var S=function(a){var c=b.items.length;return a>c-1?a-c:0>a?c+a:a},T=function(a,b,c){return a.replace(/%curr%/gi,b+1).replace(/%total%/gi,c)};a.magnificPopup.registerModule("gallery",{options:{enabled:!1,arrowMarkup:'<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>',preload:[0,2],navigateByImgClick:!0,arrows:!0,tPrev:"Previous (Left arrow key)",tNext:"Next (Right arrow key)",tCounter:"%curr% of %total%"},proto:{initGallery:function(){var c=b.st.gallery,e=".mfp-gallery";return b.direction=!0,c&&c.enabled?(f+=" mfp-gallery",w(m+e,function(){c.navigateByImgClick&&b.wrap.on("click"+e,".mfp-img",function(){return b.items.length>1?(b.next(),!1):void 0}),d.on("keydown"+e,function(a){37===a.keyCode?b.prev():39===a.keyCode&&b.next()})}),w("UpdateStatus"+e,function(a,c){c.text&&(c.text=T(c.text,b.currItem.index,b.items.length))}),w(l+e,function(a,d,e,f){var g=b.items.length;e.counter=g>1?T(c.tCounter,f.index,g):""}),w("BuildControls"+e,function(){if(b.items.length>1&&c.arrows&&!b.arrowLeft){var d=c.arrowMarkup,e=b.arrowLeft=a(d.replace(/%title%/gi,c.tPrev).replace(/%dir%/gi,"left")).addClass(s),f=b.arrowRight=a(d.replace(/%title%/gi,c.tNext).replace(/%dir%/gi,"right")).addClass(s);e.click(function(){b.prev()}),f.click(function(){b.next()}),b.container.append(e.add(f))}}),w(n+e,function(){b._preloadTimeout&&clearTimeout(b._preloadTimeout),b._preloadTimeout=setTimeout(function(){b.preloadNearbyImages(),b._preloadTimeout=null},16)}),void w(h+e,function(){d.off(e),b.wrap.off("click"+e),b.arrowRight=b.arrowLeft=null})):!1},next:function(){b.direction=!0,b.index=S(b.index+1),b.updateItemHTML()},prev:function(){b.direction=!1,b.index=S(b.index-1),b.updateItemHTML()},goTo:function(a){b.direction=a>=b.index,b.index=a,b.updateItemHTML()},preloadNearbyImages:function(){var a,c=b.st.gallery.preload,d=Math.min(c[0],b.items.length),e=Math.min(c[1],b.items.length);for(a=1;a<=(b.direction?e:d);a++)b._preloadItem(b.index+a);for(a=1;a<=(b.direction?d:e);a++)b._preloadItem(b.index-a)},_preloadItem:function(c){if(c=S(c),!b.items[c].preloaded){var d=b.items[c];d.parsed||(d=b.parseEl(c)),y("LazyLoad",d),"image"===d.type&&(d.img=a('<img class="mfp-img" />').on("load.mfploader",function(){d.hasSize=!0}).on("error.mfploader",function(){d.hasSize=!0,d.loadError=!0,y("LazyLoadError",d)}).attr("src",d.src)),d.preloaded=!0}}}});var U="retina";a.magnificPopup.registerModule(U,{options:{replaceSrc:function(a){return a.src.replace(/\.\w+$/,function(a){return"@2x"+a})},ratio:1},proto:{initRetina:function(){if(window.devicePixelRatio>1){var a=b.st.retina,c=a.ratio;c=isNaN(c)?c():c,c>1&&(w("ImageHasSize."+U,function(a,b){b.img.css({"max-width":b.img[0].naturalWidth/c,width:"100%"})}),w("ElementParse."+U,function(b,d){d.src=a.replaceSrc(d,c)}))}}}}),A()});

/*! odometer 0.4.7 */
(function(){var a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,A,B,C,D,E,F,G=[].slice;q='<span class="odometer-value"></span>',n='<span class="odometer-ribbon"><span class="odometer-ribbon-inner">'+q+"</span></span>",d='<span class="odometer-digit"><span class="odometer-digit-spacer">8</span><span class="odometer-digit-inner">'+n+"</span></span>",g='<span class="odometer-formatting-mark"></span>',c="(,ddd).dd",h=/^\(?([^)]*)\)?(?:(.)(d+))?$/,i=30,f=2e3,a=20,j=2,e=.5,k=1e3/i,b=1e3/a,o="transitionend webkitTransitionEnd oTransitionEnd otransitionend MSTransitionEnd",y=document.createElement("div").style,p=null!=y.transition||null!=y.webkitTransition||null!=y.mozTransition||null!=y.oTransition,w=window.requestAnimationFrame||window.mozRequestAnimationFrame||window.webkitRequestAnimationFrame||window.msRequestAnimationFrame,l=window.MutationObserver||window.WebKitMutationObserver||window.MozMutationObserver,s=function(a){var b;return b=document.createElement("div"),b.innerHTML=a,b.children[0]},v=function(a,b){return a.className=a.className.replace(new RegExp("(^| )"+b.split(" ").join("|")+"( |$)","gi")," ")},r=function(a,b){return v(a,b),a.className+=" "+b},z=function(a,b){var c;return null!=document.createEvent?(c=document.createEvent("HTMLEvents"),c.initEvent(b,!0,!0),a.dispatchEvent(c)):void 0},u=function(){var a,b;return null!=(a=null!=(b=window.performance)&&"function"==typeof b.now?b.now():void 0)?a:+new Date},x=function(a,b){return null==b&&(b=0),b?(a*=Math.pow(10,b),a+=.5,a=Math.floor(a),a/=Math.pow(10,b)):Math.round(a)},A=function(a){return 0>a?Math.ceil(a):Math.floor(a)},t=function(a){return a-x(a)},C=!1,(B=function(){var a,b,c,d,e;if(!C&&null!=window.jQuery){for(C=!0,d=["html","text"],e=[],b=0,c=d.length;c>b;b++)a=d[b],e.push(function(a){var b;return b=window.jQuery.fn[a],window.jQuery.fn[a]=function(a){var c;return null==a||null==(null!=(c=this[0])?c.odometer:void 0)?b.apply(this,arguments):this[0].odometer.update(a)}}(a));return e}})(),setTimeout(B,0),m=function(){function a(b){var c,d,e,g,h,i,l,m,n,o,p=this;if(this.options=b,this.el=this.options.el,null!=this.el.odometer)return this.el.odometer;this.el.odometer=this,m=a.options;for(d in m)g=m[d],null==this.options[d]&&(this.options[d]=g);null==(h=this.options).duration&&(h.duration=f),this.MAX_VALUES=this.options.duration/k/j|0,this.resetFormat(),this.value=this.cleanValue(null!=(n=this.options.value)?n:""),this.renderInside(),this.render();try{for(o=["innerHTML","innerText","textContent"],i=0,l=o.length;l>i;i++)e=o[i],null!=this.el[e]&&!function(a){return Object.defineProperty(p.el,a,{get:function(){var b;return"innerHTML"===a?p.inside.outerHTML:null!=(b=p.inside.innerText)?b:p.inside.textContent},set:function(a){return p.update(a)}})}(e)}catch(q){c=q,this.watchForMutations()}}return a.prototype.renderInside=function(){return this.inside=document.createElement("div"),this.inside.className="odometer-inside",this.el.innerHTML="",this.el.appendChild(this.inside)},a.prototype.watchForMutations=function(){var a,b=this;if(null!=l)try{return null==this.observer&&(this.observer=new l(function(){var a;return a=b.el.innerText,b.renderInside(),b.render(b.value),b.update(a)})),this.watchMutations=!0,this.startWatchingMutations()}catch(c){a=c}},a.prototype.startWatchingMutations=function(){return this.watchMutations?this.observer.observe(this.el,{childList:!0}):void 0},a.prototype.stopWatchingMutations=function(){var a;return null!=(a=this.observer)?a.disconnect():void 0},a.prototype.cleanValue=function(a){var b;return"string"==typeof a&&(a=a.replace(null!=(b=this.format.radix)?b:".","<radix>"),a=a.replace(/[.,]/g,""),a=a.replace("<radix>","."),a=parseFloat(a,10)||0),x(a,this.format.precision)},a.prototype.bindTransitionEnd=function(){var a,b,c,d,e,f,g=this;if(!this.transitionEndBound){for(this.transitionEndBound=!0,b=!1,e=o.split(" "),f=[],c=0,d=e.length;d>c;c++)a=e[c],f.push(this.el.addEventListener(a,function(){return b?!0:(b=!0,setTimeout(function(){return g.render(),b=!1,z(g.el,"odometerdone")},0),!0)},!1));return f}},a.prototype.resetFormat=function(){var a,b,d,e,f,g,i,j;if(a=null!=(i=this.options.format)?i:c,a||(a="d"),d=h.exec(a),!d)throw new Error("Odometer: Unparsable digit format");return j=d.slice(1,4),g=j[0],f=j[1],b=j[2],e=(null!=b?b.length:void 0)||0,this.format={repeating:g,radix:f,precision:e}},a.prototype.render=function(a){var b,c,d,e,f,g,h;for(null==a&&(a=this.value),this.stopWatchingMutations(),this.resetFormat(),this.inside.innerHTML="",f=this.options.theme,b=this.el.className.split(" "),e=[],g=0,h=b.length;h>g;g++)c=b[g],c.length&&((d=/^odometer-theme-(.+)$/.exec(c))?f=d[1]:/^odometer(-|$)/.test(c)||e.push(c));return e.push("odometer"),p||e.push("odometer-no-transitions"),e.push(f?"odometer-theme-"+f:"odometer-auto-theme"),this.el.className=e.join(" "),this.ribbons={},this.formatDigits(a),this.startWatchingMutations()},a.prototype.formatDigits=function(a){var b,c,d,e,f,g,h,i,j,k;if(this.digits=[],this.options.formatFunction)for(d=this.options.formatFunction(a),j=d.split("").reverse(),f=0,h=j.length;h>f;f++)c=j[f],c.match(/0-9/)?(b=this.renderDigit(),b.querySelector(".odometer-value").innerHTML=c,this.digits.push(b),this.insertDigit(b)):this.addSpacer(c);else for(e=!this.format.precision||!t(a)||!1,k=a.toString().split("").reverse(),g=0,i=k.length;i>g;g++)b=k[g],"."===b&&(e=!0),this.addDigit(b,e)},a.prototype.update=function(a){var b,c=this;return a=this.cleanValue(a),(b=a-this.value)?(v(this.el,"odometer-animating-up odometer-animating-down odometer-animating"),b>0?r(this.el,"odometer-animating-up"):r(this.el,"odometer-animating-down"),this.stopWatchingMutations(),this.animate(a),this.startWatchingMutations(),setTimeout(function(){return c.el.offsetHeight,r(c.el,"odometer-animating")},0),this.value=a):void 0},a.prototype.renderDigit=function(){return s(d)},a.prototype.insertDigit=function(a,b){return null!=b?this.inside.insertBefore(a,b):this.inside.children.length?this.inside.insertBefore(a,this.inside.children[0]):this.inside.appendChild(a)},a.prototype.addSpacer=function(a,b,c){var d;return d=s(g),d.innerHTML=a,c&&r(d,c),this.insertDigit(d,b)},a.prototype.addDigit=function(a,b){var c,d,e,f;if(null==b&&(b=!0),"-"===a)return this.addSpacer(a,null,"odometer-negation-mark");if("."===a)return this.addSpacer(null!=(f=this.format.radix)?f:".",null,"odometer-radix-mark");if(b)for(e=!1;;){if(!this.format.repeating.length){if(e)throw new Error("Bad odometer format without digits");this.resetFormat(),e=!0}if(c=this.format.repeating[this.format.repeating.length-1],this.format.repeating=this.format.repeating.substring(0,this.format.repeating.length-1),"d"===c)break;this.addSpacer(c)}return d=this.renderDigit(),d.querySelector(".odometer-value").innerHTML=a,this.digits.push(d),this.insertDigit(d)},a.prototype.animate=function(a){return p&&"count"!==this.options.animation?this.animateSlide(a):this.animateCount(a)},a.prototype.animateCount=function(a){var c,d,e,f,g,h=this;if(d=+a-this.value)return f=e=u(),c=this.value,(g=function(){var i,j,k;return u()-f>h.options.duration?(h.value=a,h.render(),void z(h.el,"odometerdone")):(i=u()-e,i>b&&(e=u(),k=i/h.options.duration,j=d*k,c+=j,h.render(Math.round(c))),null!=w?w(g):setTimeout(g,b))})()},a.prototype.getDigitCount=function(){var a,b,c,d,e,f;for(d=1<=arguments.length?G.call(arguments,0):[],a=e=0,f=d.length;f>e;a=++e)c=d[a],d[a]=Math.abs(c);return b=Math.max.apply(Math,d),Math.ceil(Math.log(b+1)/Math.log(10))},a.prototype.getFractionalDigitCount=function(){var a,b,c,d,e,f,g;for(e=1<=arguments.length?G.call(arguments,0):[],b=/^\-?\d*\.(\d*?)0*$/,a=f=0,g=e.length;g>f;a=++f)d=e[a],e[a]=d.toString(),c=b.exec(e[a]),e[a]=null==c?0:c[1].length;return Math.max.apply(Math,e)},a.prototype.resetDigits=function(){return this.digits=[],this.ribbons=[],this.inside.innerHTML="",this.resetFormat()},a.prototype.animateSlide=function(a){var b,c,d,f,g,h,i,j,k,l,m,n,o,p,q,s,t,u,v,w,x,y,z,B,C,D,E;if(s=this.value,j=this.getFractionalDigitCount(s,a),j&&(a*=Math.pow(10,j),s*=Math.pow(10,j)),d=a-s){for(this.bindTransitionEnd(),f=this.getDigitCount(s,a),g=[],b=0,m=v=0;f>=0?f>v:v>f;m=f>=0?++v:--v){if(t=A(s/Math.pow(10,f-m-1)),i=A(a/Math.pow(10,f-m-1)),h=i-t,Math.abs(h)>this.MAX_VALUES){for(l=[],n=h/(this.MAX_VALUES+this.MAX_VALUES*b*e),c=t;h>0&&i>c||0>h&&c>i;)l.push(Math.round(c)),c+=n;l[l.length-1]!==i&&l.push(i),b++}else l=function(){E=[];for(var a=t;i>=t?i>=a:a>=i;i>=t?a++:a--)E.push(a);return E}.apply(this);for(m=w=0,y=l.length;y>w;m=++w)k=l[m],l[m]=Math.abs(k%10);g.push(l)}for(this.resetDigits(),D=g.reverse(),m=x=0,z=D.length;z>x;m=++x)for(l=D[m],this.digits[m]||this.addDigit(" ",m>=j),null==(u=this.ribbons)[m]&&(u[m]=this.digits[m].querySelector(".odometer-ribbon-inner")),this.ribbons[m].innerHTML="",0>d&&(l=l.reverse()),o=C=0,B=l.length;B>C;o=++C)k=l[o],q=document.createElement("div"),q.className="odometer-value",q.innerHTML=k,this.ribbons[m].appendChild(q),o===l.length-1&&r(q,"odometer-last-value"),0===o&&r(q,"odometer-first-value");return 0>t&&this.addDigit("-"),p=this.inside.querySelector(".odometer-radix-mark"),null!=p&&p.parent.removeChild(p),j?this.addSpacer(this.format.radix,this.digits[j-1],"odometer-radix-mark"):void 0}},a}(),m.options=null!=(E=window.odometerOptions)?E:{},setTimeout(function(){var a,b,c,d,e;if(window.odometerOptions){d=window.odometerOptions,e=[];for(a in d)b=d[a],e.push(null!=(c=m.options)[a]?(c=m.options)[a]:c[a]=b);return e}},0),m.init=function(){var a,b,c,d,e,f;if(null!=document.querySelectorAll){for(b=document.querySelectorAll(m.options.selector||".odometer"),f=[],c=0,d=b.length;d>c;c++)a=b[c],f.push(a.odometer=new m({el:a,value:null!=(e=a.innerText)?e:a.textContent}));return f}},null!=(null!=(F=document.documentElement)?F.doScroll:void 0)&&null!=document.createEventObject?(D=document.onreadystatechange,document.onreadystatechange=function(){return"complete"===document.readyState&&m.options.auto!==!1&&m.init(),null!=D?D.apply(this,arguments):void 0}):document.addEventListener("DOMContentLoaded",function(){return m.options.auto!==!1?m.init():void 0},!1),"function"==typeof define&&define.amd?define(["jquery"],function(){return m}):typeof exports===!1?module.exports=m:window.Odometer=m}).call(this);

var tns=function(){var t=window,Hi=t.requestAnimationFrame||t.webkitRequestAnimationFrame||t.mozRequestAnimationFrame||t.msRequestAnimationFrame||function(t){return setTimeout(t,16)},e=window,Oi=e.cancelAnimationFrame||e.mozCancelAnimationFrame||function(t){clearTimeout(t)};function Di(){for(var t,e,n,i=arguments[0]||{},a=1,r=arguments.length;a<r;a++)if(null!==(t=arguments[a]))for(e in t)i!==(n=t[e])&&void 0!==n&&(i[e]=n);return i}function ki(t){return 0<=["true","false"].indexOf(t)?JSON.parse(t):t}function Ri(t,e,n,i){if(i)try{t.setItem(e,n)}catch(t){}return n}function Ii(){var t=document,e=t.body;return e||((e=t.createElement("body")).fake=!0),e}var n=document.documentElement;function Pi(t){var e="";return t.fake&&(e=n.style.overflow,t.style.background="",t.style.overflow=n.style.overflow="hidden",n.appendChild(t)),e}function zi(t,e){t.fake&&(t.remove(),n.style.overflow=e,n.offsetHeight)}function Wi(t,e,n,i){"insertRule"in t?t.insertRule(e+"{"+n+"}",i):t.addRule(e,n,i)}function qi(t){return("insertRule"in t?t.cssRules:t.rules).length}function Fi(t,e,n){for(var i=0,a=t.length;i<a;i++)e.call(n,t[i],i)}var i="classList"in document.createElement("_"),ji=i?function(t,e){return t.classList.contains(e)}:function(t,e){return 0<=t.className.indexOf(e)},Vi=i?function(t,e){ji(t,e)||t.classList.add(e)}:function(t,e){ji(t,e)||(t.className+=" "+e)},Gi=i?function(t,e){ji(t,e)&&t.classList.remove(e)}:function(t,e){ji(t,e)&&(t.className=t.className.replace(e,""))};function Qi(t,e){return t.hasAttribute(e)}function Xi(t,e){return t.getAttribute(e)}function r(t){return void 0!==t.item}function Yi(t,e){if(t=r(t)||t instanceof Array?t:[t],"[object Object]"===Object.prototype.toString.call(e))for(var n=t.length;n--;)for(var i in e)t[n].setAttribute(i,e[i])}function Ki(t,e){t=r(t)||t instanceof Array?t:[t];for(var n=(e=e instanceof Array?e:[e]).length,i=t.length;i--;)for(var a=n;a--;)t[i].removeAttribute(e[a])}function Ji(t){for(var e=[],n=0,i=t.length;n<i;n++)e.push(t[n]);return e}function Ui(t,e){"none"!==t.style.display&&(t.style.display="none")}function _i(t,e){"none"===t.style.display&&(t.style.display="")}function Zi(t){return"none"!==window.getComputedStyle(t).display}function $i(e){if("string"==typeof e){var n=[e],i=e.charAt(0).toUpperCase()+e.substr(1);["Webkit","Moz","ms","O"].forEach(function(t){"ms"===t&&"transform"!==e||n.push(t+i)}),e=n}for(var t=document.createElement("fakeelement"),a=(e.length,0);a<e.length;a++){var r=e[a];if(void 0!==t.style[r])return r}return!1}function ta(t,e){var n=!1;return/^Webkit/.test(t)?n="webkit"+e+"End":/^O/.test(t)?n="o"+e+"End":t&&(n=e.toLowerCase()+"end"),n}var a=!1;try{var o=Object.defineProperty({},"passive",{get:function(){a=!0}});window.addEventListener("test",null,o)}catch(t){}var u=!!a&&{passive:!0};function ea(t,e,n){for(var i in e){var a=0<=["touchstart","touchmove"].indexOf(i)&&!n&&u;t.addEventListener(i,e[i],a)}}function na(t,e){for(var n in e){var i=0<=["touchstart","touchmove"].indexOf(n)&&u;t.removeEventListener(n,e[n],i)}}function ia(){return{topics:{},on:function(t,e){this.topics[t]=this.topics[t]||[],this.topics[t].push(e)},off:function(t,e){if(this.topics[t])for(var n=0;n<this.topics[t].length;n++)if(this.topics[t][n]===e){this.topics[t].splice(n,1);break}},emit:function(e,n){n.type=e,this.topics[e]&&this.topics[e].forEach(function(t){t(n,e)})}}}Object.keys||(Object.keys=function(t){var e=[];for(var n in t)Object.prototype.hasOwnProperty.call(t,n)&&e.push(n);return e}),"remove"in Element.prototype||(Element.prototype.remove=function(){this.parentNode&&this.parentNode.removeChild(this)});var aa=function(H){H=Di({container:".slider",mode:"carousel",axis:"horizontal",items:1,gutter:0,edgePadding:0,fixedWidth:!1,autoWidth:!1,viewportMax:!1,slideBy:1,center:!1,controls:!0,controlsPosition:"top",controlsText:["prev","next"],controlsContainer:!1,prevButton:!1,nextButton:!1,nav:!0,navPosition:"top",navContainer:!1,navAsThumbnails:!1,arrowKeys:!1,speed:300,autoplay:!1,autoplayPosition:"top",autoplayTimeout:5e3,autoplayDirection:"forward",autoplayText:["start","stop"],autoplayHoverPause:!1,autoplayButton:!1,autoplayButtonOutput:!0,autoplayResetOnVisibility:!0,animateIn:"tns-fadeIn",animateOut:"tns-fadeOut",animateNormal:"tns-normal",animateDelay:!1,loop:!0,rewind:!1,autoHeight:!1,responsive:!1,lazyload:!1,lazyloadSelector:".tns-lazy-img",touch:!0,mouseDrag:!1,swipeAngle:15,nested:!1,preventActionWhenRunning:!1,preventScrollOnTouch:!1,freezable:!0,onInit:!1,useLocalStorage:!0},H||{});var O=document,m=window,a={ENTER:13,SPACE:32,LEFT:37,RIGHT:39},e={},n=H.useLocalStorage;if(n){var t=navigator.userAgent,i=new Date;try{(e=m.localStorage)?(e.setItem(i,i),n=e.getItem(i)==i,e.removeItem(i)):n=!1,n||(e={})}catch(t){n=!1}n&&(e.tnsApp&&e.tnsApp!==t&&["tC","tPL","tMQ","tTf","t3D","tTDu","tTDe","tADu","tADe","tTE","tAE"].forEach(function(t){e.removeItem(t)}),localStorage.tnsApp=t)}var r,o,u,l,s,c,f,y=e.tC?ki(e.tC):Ri(e,"tC",function(){var t=document,e=Ii(),n=Pi(e),i=t.createElement("div"),a=!1;e.appendChild(i);try{for(var r,o="(10px * 10)",u=["calc"+o,"-moz-calc"+o,"-webkit-calc"+o],l=0;l<3;l++)if(r=u[l],i.style.width=r,100===i.offsetWidth){a=r.replace(o,"");break}}catch(t){}return e.fake?zi(e,n):i.remove(),a}(),n),g=e.tPL?ki(e.tPL):Ri(e,"tPL",function(){var t,e=document,n=Ii(),i=Pi(n),a=e.createElement("div"),r=e.createElement("div"),o="";a.className="tns-t-subp2",r.className="tns-t-ct";for(var u=0;u<70;u++)o+="<div></div>";return r.innerHTML=o,a.appendChild(r),n.appendChild(a),t=Math.abs(a.getBoundingClientRect().left-r.children[67].getBoundingClientRect().left)<2,n.fake?zi(n,i):a.remove(),t}(),n),D=e.tMQ?ki(e.tMQ):Ri(e,"tMQ",(o=document,u=Ii(),l=Pi(u),s=o.createElement("div"),c=o.createElement("style"),f="@media all and (min-width:1px){.tns-mq-test{position:absolute}}",c.type="text/css",s.className="tns-mq-test",u.appendChild(c),u.appendChild(s),c.styleSheet?c.styleSheet.cssText=f:c.appendChild(o.createTextNode(f)),r=window.getComputedStyle?window.getComputedStyle(s).position:s.currentStyle.position,u.fake?zi(u,l):s.remove(),"absolute"===r),n),d=e.tTf?ki(e.tTf):Ri(e,"tTf",$i("transform"),n),v=e.t3D?ki(e.t3D):Ri(e,"t3D",function(t){if(!t)return!1;if(!window.getComputedStyle)return!1;var e,n=document,i=Ii(),a=Pi(i),r=n.createElement("p"),o=9<t.length?"-"+t.slice(0,-9).toLowerCase()+"-":"";return o+="transform",i.insertBefore(r,null),r.style[t]="translate3d(1px,1px,1px)",e=window.getComputedStyle(r).getPropertyValue(o),i.fake?zi(i,a):r.remove(),void 0!==e&&0<e.length&&"none"!==e}(d),n),x=e.tTDu?ki(e.tTDu):Ri(e,"tTDu",$i("transitionDuration"),n),p=e.tTDe?ki(e.tTDe):Ri(e,"tTDe",$i("transitionDelay"),n),b=e.tADu?ki(e.tADu):Ri(e,"tADu",$i("animationDuration"),n),h=e.tADe?ki(e.tADe):Ri(e,"tADe",$i("animationDelay"),n),C=e.tTE?ki(e.tTE):Ri(e,"tTE",ta(x,"Transition"),n),w=e.tAE?ki(e.tAE):Ri(e,"tAE",ta(b,"Animation"),n),M=m.console&&"function"==typeof m.console.warn,T=["container","controlsContainer","prevButton","nextButton","navContainer","autoplayButton"],E={};if(T.forEach(function(t){if("string"==typeof H[t]){var e=H[t],n=O.querySelector(e);if(E[t]=e,!n||!n.nodeName)return void(M&&console.warn("Can't find",H[t]));H[t]=n}}),!(H.container.children.length<1)){var k=H.responsive,R=H.nested,I="carousel"===H.mode;if(k){0 in k&&(H=Di(H,k[0]),delete k[0]);var A={};for(var N in k){var L=k[N];L="number"==typeof L?{items:L}:L,A[N]=L}k=A,A=null}if(I||function t(e){for(var n in e)I||("slideBy"===n&&(e[n]="page"),"edgePadding"===n&&(e[n]=!1),"autoHeight"===n&&(e[n]=!1)),"responsive"===n&&t(e[n])}(H),!I){H.axis="horizontal",H.slideBy="page",H.edgePadding=!1;var P=H.animateIn,z=H.animateOut,B=H.animateDelay,W=H.animateNormal}var S,q,F="horizontal"===H.axis,j=O.createElement("div"),V=O.createElement("div"),G=H.container,Q=G.parentNode,X=G.outerHTML,Y=G.children,K=Y.length,J=cn(),U=!1;k&&Sn(),I&&(G.className+=" tns-vpfix");var _,Z,$,tt,et,nt,it,at,rt=H.autoWidth,ot=pn("fixedWidth"),ut=pn("edgePadding"),lt=pn("gutter"),st=dn(),ct=pn("center"),ft=rt?1:Math.floor(pn("items")),dt=pn("slideBy"),vt=H.viewportMax||H.fixedWidthViewportWidth,pt=pn("arrowKeys"),ht=pn("speed"),mt=H.rewind,yt=!mt&&H.loop,gt=pn("autoHeight"),xt=pn("controls"),bt=pn("controlsText"),Ct=pn("nav"),wt=pn("touch"),Mt=pn("mouseDrag"),Tt=pn("autoplay"),Et=pn("autoplayTimeout"),At=pn("autoplayText"),Nt=pn("autoplayHoverPause"),Lt=pn("autoplayResetOnVisibility"),Bt=(at=document.createElement("style"),it&&at.setAttribute("media",it),document.querySelector("head").appendChild(at),at.sheet?at.sheet:at.styleSheet),St=H.lazyload,Ht=H.lazyloadSelector,Ot=[],Dt=yt?(et=function(){{if(rt||ot&&!vt)return K-1;var t=ot?"fixedWidth":"items",e=[];if((ot||H[t]<K)&&e.push(H[t]),k)for(var n in k){var i=k[n][t];i&&(ot||i<K)&&e.push(i)}return e.length||e.push(0),Math.ceil(ot?vt/Math.min.apply(null,e):Math.max.apply(null,e))}}(),nt=I?Math.ceil((5*et-K)/2):4*et-K,nt=Math.max(et,nt),vn("edgePadding")?nt+1:nt):0,kt=I?K+2*Dt:K+Dt,Rt=!(!ot&&!rt||yt),It=ot?ni():null,Pt=!I||!yt,zt=F?"left":"top",Wt="",qt="",Ft=ot?function(){return ct&&!yt?K-1:Math.ceil(-It/(ot+lt))}:rt?function(){for(var t=kt;t--;)if(_[t]>=-It)return t}:function(){return ct&&I&&!yt?K-1:yt||I?Math.max(0,kt-Math.ceil(ft)):kt-1},jt=un(pn("startIndex")),Vt=jt,Gt=(on(),0),Qt=rt?null:Ft(),Xt=H.preventActionWhenRunning,Yt=H.swipeAngle,Kt=!Yt||"?",Jt=!1,Ut=H.onInit,_t=new ia,Zt=" tns-slider tns-"+H.mode,$t=G.id||(tt=window.tnsId,window.tnsId=tt?tt+1:1,"tns"+window.tnsId),te=pn("disable"),ee=!1,ne=H.freezable,ie=!(!ne||rt)&&Bn(),ae=!1,re={click:fi,keydown:function(t){t=xi(t);var e=[a.LEFT,a.RIGHT].indexOf(t.keyCode);0<=e&&(0===e?Ae.disabled||fi(t,-1):Ne.disabled||fi(t,1))}},oe={click:function(t){if(Jt){if(Xt)return;si()}var e=bi(t=xi(t));for(;e!==He&&!Qi(e,"data-nav");)e=e.parentNode;if(Qi(e,"data-nav")){var n=Re=Number(Xi(e,"data-nav")),i=ot||rt?n*K/De:n*ft,a=pe?n:Math.min(Math.ceil(i),K-1);ci(a,t),Ie===n&&(je&&mi(),Re=-1)}},keydown:function(t){t=xi(t);var e=O.activeElement;if(!Qi(e,"data-nav"))return;var n=[a.LEFT,a.RIGHT,a.ENTER,a.SPACE].indexOf(t.keyCode),i=Number(Xi(e,"data-nav"));0<=n&&(0===n?0<i&&gi(Se[i-1]):1===n?i<De-1&&gi(Se[i+1]):ci(Re=i,t))}},ue={mouseover:function(){je&&(vi(),Ve=!0)},mouseout:function(){Ve&&(di(),Ve=!1)}},le={visibilitychange:function(){O.hidden?je&&(vi(),Qe=!0):Qe&&(di(),Qe=!1)}},se={keydown:function(t){t=xi(t);var e=[a.LEFT,a.RIGHT].indexOf(t.keyCode);0<=e&&fi(t,0===e?-1:1)}},ce={touchstart:Ti,touchmove:Ei,touchend:Ai,touchcancel:Ai},fe={mousedown:Ti,mousemove:Ei,mouseup:Ai,mouseleave:Ai},de=vn("controls"),ve=vn("nav"),pe=!!rt||H.navAsThumbnails,he=vn("autoplay"),me=vn("touch"),ye=vn("mouseDrag"),ge="tns-slide-active",xe="tns-complete",be={load:function(t){Wn(bi(t))},error:function(t){e=bi(t),Vi(e,"failed"),qn(e);var e}},Ce="force"===H.preventScrollOnTouch;if(de)var we,Me,Te=H.controlsContainer,Ee=H.controlsContainer?H.controlsContainer.outerHTML:"",Ae=H.prevButton,Ne=H.nextButton,Le=H.prevButton?H.prevButton.outerHTML:"",Be=H.nextButton?H.nextButton.outerHTML:"";if(ve)var Se,He=H.navContainer,Oe=H.navContainer?H.navContainer.outerHTML:"",De=rt?K:Li(),ke=0,Re=-1,Ie=sn(),Pe=Ie,ze="tns-nav-active",We="Carousel Page ",qe=" (Current Slide)";if(he)var Fe,je,Ve,Ge,Qe,Xe="forward"===H.autoplayDirection?1:-1,Ye=H.autoplayButton,Ke=H.autoplayButton?H.autoplayButton.outerHTML:"",Je=["<span class='tns-visually-hidden'>"," animation</span>"];if(me||ye)var Ue,_e,Ze={},$e={},tn=!1,en=F?function(t,e){return t.x-e.x}:function(t,e){return t.y-e.y};rt||rn(te||ie),d&&(zt=d,Wt="translate",v?(Wt+=F?"3d(":"3d(0px, ",qt=F?", 0px, 0px)":", 0px)"):(Wt+=F?"X(":"Y(",qt=")")),I&&(G.className=G.className.replace("tns-vpfix","")),function(){vn("gutter");j.className="tns-outer",V.className="tns-inner",j.id=$t+"-ow",V.id=$t+"-iw",""===G.id&&(G.id=$t);Zt+=g||rt?" tns-subpixel":" tns-no-subpixel",Zt+=y?" tns-calc":" tns-no-calc",rt&&(Zt+=" tns-autowidth");Zt+=" tns-"+H.axis,G.className+=Zt,I?((S=O.createElement("div")).id=$t+"-mw",S.className="tns-ovh",j.appendChild(S),S.appendChild(V)):j.appendChild(V);if(gt){var t=S||V;t.className+=" tns-ah"}if(Q.insertBefore(j,G),V.appendChild(G),Fi(Y,function(t,e){Vi(t,"tns-item"),t.id||(t.id=$t+"-item"+e),!I&&W&&Vi(t,W),Yi(t,{"aria-hidden":"true",tabindex:"-1"})}),Dt){for(var e=O.createDocumentFragment(),n=O.createDocumentFragment(),i=Dt;i--;){var a=i%K,r=Y[a].cloneNode(!0);if(Ki(r,"id"),n.insertBefore(r,n.firstChild),I){var o=Y[K-1-a].cloneNode(!0);Ki(o,"id"),e.appendChild(o)}}G.insertBefore(e,G.firstChild),G.appendChild(n),Y=G.children}}(),function(){if(!I)for(var t=jt,e=jt+Math.min(K,ft);t<e;t++){var n=Y[t];n.style.left=100*(t-jt)/ft+"%",Vi(n,P),Gi(n,W)}F&&(g||rt?(Wi(Bt,"#"+$t+" > .tns-item","font-size:"+m.getComputedStyle(Y[0]).fontSize+";",qi(Bt)),Wi(Bt,"#"+$t,"font-size:0;",qi(Bt))):I&&Fi(Y,function(t,e){var n;t.style.marginLeft=(n=e,y?y+"("+100*n+"% / "+kt+")":100*n/kt+"%")}));if(D){if(x){var i=S&&H.autoHeight?bn(H.speed):"";Wi(Bt,"#"+$t+"-mw",i,qi(Bt))}i=hn(H.edgePadding,H.gutter,H.fixedWidth,H.speed,H.autoHeight),Wi(Bt,"#"+$t+"-iw",i,qi(Bt)),I&&(i=F&&!rt?"width:"+mn(H.fixedWidth,H.gutter,H.items)+";":"",x&&(i+=bn(ht)),Wi(Bt,"#"+$t,i,qi(Bt))),i=F&&!rt?yn(H.fixedWidth,H.gutter,H.items):"",H.gutter&&(i+=gn(H.gutter)),I||(x&&(i+=bn(ht)),b&&(i+=Cn(ht))),i&&Wi(Bt,"#"+$t+" > .tns-item",i,qi(Bt))}else{I&&gt&&(S.style[x]=ht/1e3+"s"),V.style.cssText=hn(ut,lt,ot,gt),I&&F&&!rt&&(G.style.width=mn(ot,lt,ft));var i=F&&!rt?yn(ot,lt,ft):"";lt&&(i+=gn(lt)),i&&Wi(Bt,"#"+$t+" > .tns-item",i,qi(Bt))}if(k&&D)for(var a in k){a=parseInt(a);var r=k[a],i="",o="",u="",l="",s="",c=rt?null:pn("items",a),f=pn("fixedWidth",a),d=pn("speed",a),v=pn("edgePadding",a),p=pn("autoHeight",a),h=pn("gutter",a);x&&S&&pn("autoHeight",a)&&"speed"in r&&(o="#"+$t+"-mw{"+bn(d)+"}"),("edgePadding"in r||"gutter"in r)&&(u="#"+$t+"-iw{"+hn(v,h,f,d,p)+"}"),I&&F&&!rt&&("fixedWidth"in r||"items"in r||ot&&"gutter"in r)&&(l="width:"+mn(f,h,c)+";"),x&&"speed"in r&&(l+=bn(d)),l&&(l="#"+$t+"{"+l+"}"),("fixedWidth"in r||ot&&"gutter"in r||!I&&"items"in r)&&(s+=yn(f,h,c)),"gutter"in r&&(s+=gn(h)),!I&&"speed"in r&&(x&&(s+=bn(d)),b&&(s+=Cn(d))),s&&(s="#"+$t+" > .tns-item{"+s+"}"),(i=o+u+l+s)&&Bt.insertRule("@media (min-width: "+a/16+"em) {"+i+"}",Bt.cssRules.length)}}(),wn();var nn=yt?I?function(){var t=Gt,e=Qt;t+=dt,e-=dt,ut?(t+=1,e-=1):ot&&(st+lt)%(ot+lt)&&(e-=1),Dt&&(e<jt?jt-=K:jt<t&&(jt+=K))}:function(){if(Qt<jt)for(;Gt+K<=jt;)jt-=K;else if(jt<Gt)for(;jt<=Qt-K;)jt+=K}:function(){jt=Math.max(Gt,Math.min(Qt,jt))},an=I?function(){var e,n,i,a,t,r,o,u,l,s,c;ti(G,""),x||!ht?(ri(),ht&&Zi(G)||si()):(e=G,n=zt,i=Wt,a=qt,t=ii(),r=ht,o=si,u=Math.min(r,10),l=0<=t.indexOf("%")?"%":"px",t=t.replace(l,""),s=Number(e.style[n].replace(i,"").replace(a,"").replace(l,"")),c=(t-s)/r*u,setTimeout(function t(){r-=u,s+=c,e.style[n]=i+s+l+a,0<r?setTimeout(t,u):o()},u)),F||Ni()}:function(){Ot=[];var t={};t[C]=t[w]=si,na(Y[Vt],t),ea(Y[jt],t),oi(Vt,P,z,!0),oi(jt,W,P),C&&w&&ht&&Zi(G)||si()};return{version:"2.9.2",getInfo:Si,events:_t,goTo:ci,play:function(){Tt&&!je&&(hi(),Ge=!1)},pause:function(){je&&(mi(),Ge=!0)},isOn:U,updateSliderHeight:Xn,refresh:wn,destroy:function(){if(Bt.disabled=!0,Bt.ownerNode&&Bt.ownerNode.remove(),na(m,{resize:Nn}),pt&&na(O,se),Te&&na(Te,re),He&&na(He,oe),na(G,ue),na(G,le),Ye&&na(Ye,{click:yi}),Tt&&clearInterval(Fe),I&&C){var t={};t[C]=si,na(G,t)}wt&&na(G,ce),Mt&&na(G,fe);var r=[X,Ee,Le,Be,Oe,Ke];for(var e in T.forEach(function(t,e){var n="container"===t?j:H[t];if("object"==typeof n&&n){var i=!!n.previousElementSibling&&n.previousElementSibling,a=n.parentNode;n.outerHTML=r[e],H[t]=i?i.nextElementSibling:a.firstElementChild}}),T=P=z=B=W=F=j=V=G=Q=X=Y=K=q=J=rt=ot=ut=lt=st=ft=dt=vt=pt=ht=mt=yt=gt=Bt=St=_=Ot=Dt=kt=Rt=It=Pt=zt=Wt=qt=Ft=jt=Vt=Gt=Qt=Yt=Kt=Jt=Ut=_t=Zt=$t=te=ee=ne=ie=ae=re=oe=ue=le=se=ce=fe=de=ve=pe=he=me=ye=ge=xe=be=Z=xt=bt=Te=Ee=Ae=Ne=we=Me=Ct=He=Oe=Se=De=ke=Re=Ie=Pe=ze=We=qe=Tt=Et=Xe=At=Nt=Ye=Ke=Lt=Je=Fe=je=Ve=Ge=Qe=Ze=$e=Ue=tn=_e=en=wt=Mt=null,this)"rebuild"!==e&&(this[e]=null);U=!1},rebuild:function(){return aa(Di(H,E))}}}function rn(t){t&&(xt=Ct=wt=Mt=pt=Tt=Nt=Lt=!1)}function on(){for(var t=I?jt-Dt:jt;t<0;)t+=K;return t%K+1}function un(t){return t=t?Math.max(0,Math.min(yt?K-1:K-ft,t)):0,I?t+Dt:t}function ln(t){for(null==t&&(t=jt),I&&(t-=Dt);t<0;)t+=K;return Math.floor(t%K)}function sn(){var t,e=ln();return t=pe?e:ot||rt?Math.ceil((e+1)*De/K-1):Math.floor(e/ft),!yt&&I&&jt===Qt&&(t=De-1),t}function cn(){return m.innerWidth||O.documentElement.clientWidth||O.body.clientWidth}function fn(t){return"top"===t?"afterbegin":"beforeend"}function dn(){var t=ut?2*ut-lt:0;return function t(e){if(null!=e){var n,i,a=O.createElement("div");return e.appendChild(a),i=(n=a.getBoundingClientRect()).right-n.left,a.remove(),i||t(e.parentNode)}}(Q)-t}function vn(t){if(H[t])return!0;if(k)for(var e in k)if(k[e][t])return!0;return!1}function pn(t,e){if(null==e&&(e=J),"items"===t&&ot)return Math.floor((st+lt)/(ot+lt))||1;var n=H[t];if(k)for(var i in k)e>=parseInt(i)&&t in k[i]&&(n=k[i][t]);return"slideBy"===t&&"page"===n&&(n=pn("items")),I||"slideBy"!==t&&"items"!==t||(n=Math.floor(n)),n}function hn(t,e,n,i,a){var r="";if(void 0!==t){var o=t;e&&(o-=e),r=F?"margin: 0 "+o+"px 0 "+t+"px;":"margin: "+t+"px 0 "+o+"px 0;"}else if(e&&!n){var u="-"+e+"px";r="margin: 0 "+(F?u+" 0 0":"0 "+u+" 0")+";"}return!I&&a&&x&&i&&(r+=bn(i)),r}function mn(t,e,n){return t?(t+e)*kt+"px":y?y+"("+100*kt+"% / "+n+")":100*kt/n+"%"}function yn(t,e,n){var i;if(t)i=t+e+"px";else{I||(n=Math.floor(n));var a=I?kt:n;i=y?y+"(100% / "+a+")":100/a+"%"}return i="width:"+i,"inner"!==R?i+";":i+" !important;"}function gn(t){var e="";!1!==t&&(e=(F?"padding-":"margin-")+(F?"right":"bottom")+": "+t+"px;");return e}function xn(t,e){var n=t.substring(0,t.length-e).toLowerCase();return n&&(n="-"+n+"-"),n}function bn(t){return xn(x,18)+"transition-duration:"+t/1e3+"s;"}function Cn(t){return xn(b,17)+"animation-duration:"+t/1e3+"s;"}function wn(){if(vn("autoHeight")||rt||!F){var t=G.querySelectorAll("img");Fi(t,function(t){var e=t.src;St||(e&&e.indexOf("data:image")<0?(t.src="",ea(t,be),Vi(t,"loading"),t.src=e):Wn(t))}),Hi(function(){Vn(Ji(t),function(){Z=!0})}),vn("autoHeight")&&(t=Fn(jt,Math.min(jt+ft-1,kt-1))),St?Mn():Hi(function(){Vn(Ji(t),Mn)})}else I&&ai(),En(),An()}function Mn(){if(rt){var i=yt?jt:K-1;!function t(){var e=Y[i].getBoundingClientRect().left,n=Y[i-1].getBoundingClientRect().right;Math.abs(e-n)<=1?Tn():setTimeout(function(){t()},16)}()}else Tn()}function Tn(){F&&!rt||(Yn(),rt?(It=ni(),ne&&(ie=Bn()),Qt=Ft(),rn(te||ie)):Ni()),I&&ai(),En(),An()}function En(){if(Kn(),j.insertAdjacentHTML("afterbegin",'<div class="tns-liveregion tns-visually-hidden" aria-live="polite" aria-atomic="true">slide <span class="current">'+In()+"</span>  of "+K+"</div>"),$=j.querySelector(".tns-liveregion .current"),he){var t=Tt?"stop":"start";Ye?Yi(Ye,{"data-action":t}):H.autoplayButtonOutput&&(j.insertAdjacentHTML(fn(H.autoplayPosition),'<button type="button" data-action="'+t+'">'+Je[0]+t+Je[1]+At[0]+"</button>"),Ye=j.querySelector("[data-action]")),Ye&&ea(Ye,{click:yi}),Tt&&(hi(),Nt&&ea(G,ue),Lt&&ea(G,le))}if(ve){if(He)Yi(He,{"aria-label":"Carousel Pagination"}),Fi(Se=He.children,function(t,e){Yi(t,{"data-nav":e,tabindex:"-1","aria-label":We+(e+1),"aria-controls":$t})});else{for(var e="",n=pe?"":'style="display:none"',i=0;i<K;i++)e+='<button type="button" data-nav="'+i+'" tabindex="-1" aria-controls="'+$t+'" '+n+' aria-label="'+We+(i+1)+'"></button>';e='<div class="tns-nav" aria-label="Carousel Pagination">'+e+"</div>",j.insertAdjacentHTML(fn(H.navPosition),e),He=j.querySelector(".tns-nav"),Se=He.children}if(Bi(),x){var a=x.substring(0,x.length-18).toLowerCase(),r="transition: all "+ht/1e3+"s";a&&(r="-"+a+"-"+r),Wi(Bt,"[aria-controls^="+$t+"-item]",r,qi(Bt))}Yi(Se[Ie],{"aria-label":We+(Ie+1)+qe}),Ki(Se[Ie],"tabindex"),Vi(Se[Ie],ze),ea(He,oe)}de&&(Te||Ae&&Ne||(j.insertAdjacentHTML(fn(H.controlsPosition),'<div class="tns-controls" aria-label="Carousel Navigation" tabindex="0"><button type="button" data-controls="prev" tabindex="-1" aria-controls="'+$t+'">'+bt[0]+'</button><button type="button" data-controls="next" tabindex="-1" aria-controls="'+$t+'">'+bt[1]+"</button></div>"),Te=j.querySelector(".tns-controls")),Ae&&Ne||(Ae=Te.children[0],Ne=Te.children[1]),H.controlsContainer&&Yi(Te,{"aria-label":"Carousel Navigation",tabindex:"0"}),(H.controlsContainer||H.prevButton&&H.nextButton)&&Yi([Ae,Ne],{"aria-controls":$t,tabindex:"-1"}),(H.controlsContainer||H.prevButton&&H.nextButton)&&(Yi(Ae,{"data-controls":"prev"}),Yi(Ne,{"data-controls":"next"})),we=Un(Ae),Me=Un(Ne),$n(),Te?ea(Te,re):(ea(Ae,re),ea(Ne,re))),Hn()}function An(){if(I&&C){var t={};t[C]=si,ea(G,t)}wt&&ea(G,ce,H.preventScrollOnTouch),Mt&&ea(G,fe),pt&&ea(O,se),"inner"===R?_t.on("outerResized",function(){Ln(),_t.emit("innerLoaded",Si())}):(k||ot||rt||gt||!F)&&ea(m,{resize:Nn}),gt&&("outer"===R?_t.on("innerLoaded",jn):te||jn()),zn(),te?kn():ie&&Dn(),_t.on("indexChanged",Gn),"inner"===R&&_t.emit("innerLoaded",Si()),"function"==typeof Ut&&Ut(Si()),U=!0}function Nn(t){Hi(function(){Ln(xi(t))})}function Ln(t){if(U){"outer"===R&&_t.emit("outerResized",Si(t)),J=cn();var e,n=q,i=!1;k&&(Sn(),(e=n!==q)&&_t.emit("newBreakpointStart",Si(t)));var a,r,o,u,l=ft,s=te,c=ie,f=pt,d=xt,v=Ct,p=wt,h=Mt,m=Tt,y=Nt,g=Lt,x=jt;if(e){var b=ot,C=gt,w=bt,M=ct,T=At;if(!D)var E=lt,A=ut}if(pt=pn("arrowKeys"),xt=pn("controls"),Ct=pn("nav"),wt=pn("touch"),ct=pn("center"),Mt=pn("mouseDrag"),Tt=pn("autoplay"),Nt=pn("autoplayHoverPause"),Lt=pn("autoplayResetOnVisibility"),e&&(te=pn("disable"),ot=pn("fixedWidth"),ht=pn("speed"),gt=pn("autoHeight"),bt=pn("controlsText"),At=pn("autoplayText"),Et=pn("autoplayTimeout"),D||(ut=pn("edgePadding"),lt=pn("gutter"))),rn(te),st=dn(),F&&!rt||te||(Yn(),F||(Ni(),i=!0)),(ot||rt)&&(It=ni(),Qt=Ft()),(e||ot)&&(ft=pn("items"),dt=pn("slideBy"),(r=ft!==l)&&(ot||rt||(Qt=Ft()),nn())),e&&te!==s&&(te?kn():function(){if(!ee)return;if(Bt.disabled=!1,G.className+=Zt,ai(),yt)for(var t=Dt;t--;)I&&_i(Y[t]),_i(Y[kt-t-1]);if(!I)for(var e=jt,n=jt+K;e<n;e++){var i=Y[e],a=e<jt+ft?P:W;i.style.left=100*(e-jt)/ft+"%",Vi(i,a)}On(),ee=!1}()),ne&&(e||ot||rt)&&(ie=Bn())!==c&&(ie?(ri(ii(un(0))),Dn()):(!function(){if(!ae)return;ut&&D&&(V.style.margin="");if(Dt)for(var t="tns-transparent",e=Dt;e--;)I&&Gi(Y[e],t),Gi(Y[kt-e-1],t);On(),ae=!1}(),i=!0)),rn(te||ie),Tt||(Nt=Lt=!1),pt!==f&&(pt?ea(O,se):na(O,se)),xt!==d&&(xt?Te?_i(Te):(Ae&&_i(Ae),Ne&&_i(Ne)):Te?Ui(Te):(Ae&&Ui(Ae),Ne&&Ui(Ne))),Ct!==v&&(Ct?_i(He):Ui(He)),wt!==p&&(wt?ea(G,ce,H.preventScrollOnTouch):na(G,ce)),Mt!==h&&(Mt?ea(G,fe):na(G,fe)),Tt!==m&&(Tt?(Ye&&_i(Ye),je||Ge||hi()):(Ye&&Ui(Ye),je&&mi())),Nt!==y&&(Nt?ea(G,ue):na(G,ue)),Lt!==g&&(Lt?ea(O,le):na(O,le)),e){if(ot===b&&ct===M||(i=!0),gt!==C&&(gt||(V.style.height="")),xt&&bt!==w&&(Ae.innerHTML=bt[0],Ne.innerHTML=bt[1]),Ye&&At!==T){var N=Tt?1:0,L=Ye.innerHTML,B=L.length-T[N].length;L.substring(B)===T[N]&&(Ye.innerHTML=L.substring(0,B)+At[N])}}else ct&&(ot||rt)&&(i=!0);if((r||ot&&!rt)&&(De=Li(),Bi()),(a=jt!==x)?(_t.emit("indexChanged",Si()),i=!0):r?a||Gn():(ot||rt)&&(zn(),Kn(),Rn()),r&&!I&&function(){for(var t=jt+Math.min(K,ft),e=kt;e--;){var n=Y[e];jt<=e&&e<t?(Vi(n,"tns-moving"),n.style.left=100*(e-jt)/ft+"%",Vi(n,P),Gi(n,W)):n.style.left&&(n.style.left="",Vi(n,W),Gi(n,P)),Gi(n,z)}setTimeout(function(){Fi(Y,function(t){Gi(t,"tns-moving")})},300)}(),!te&&!ie){if(e&&!D&&(ut===A&&lt===E||(V.style.cssText=hn(ut,lt,ot,ht,gt)),F)){I&&(G.style.width=mn(ot,lt,ft));var S=yn(ot,lt,ft)+gn(lt);u=qi(o=Bt)-1,"deleteRule"in o?o.deleteRule(u):o.removeRule(u),Wi(Bt,"#"+$t+" > .tns-item",S,qi(Bt))}gt&&jn(),i&&(ai(),Vt=jt)}e&&_t.emit("newBreakpointEnd",Si(t))}}function Bn(){if(!ot&&!rt)return K<=(ct?ft-(ft-1)/2:ft);var t=ot?(ot+lt)*K:_[K],e=ut?st+2*ut:st+lt;return ct&&(e-=ot?(st-ot)/2:(st-(_[jt+1]-_[jt]-lt))/2),t<=e}function Sn(){for(var t in q=0,k)(t=parseInt(t))<=J&&(q=t)}function Hn(){!Tt&&Ye&&Ui(Ye),!Ct&&He&&Ui(He),xt||(Te?Ui(Te):(Ae&&Ui(Ae),Ne&&Ui(Ne)))}function On(){Tt&&Ye&&_i(Ye),Ct&&He&&_i(He),xt&&(Te?_i(Te):(Ae&&_i(Ae),Ne&&_i(Ne)))}function Dn(){if(!ae){if(ut&&(V.style.margin="0px"),Dt)for(var t="tns-transparent",e=Dt;e--;)I&&Vi(Y[e],t),Vi(Y[kt-e-1],t);Hn(),ae=!0}}function kn(){if(!ee){if(Bt.disabled=!0,G.className=G.className.replace(Zt.substring(1),""),Ki(G,["style"]),yt)for(var t=Dt;t--;)I&&Ui(Y[t]),Ui(Y[kt-t-1]);if(F&&I||Ki(V,["style"]),!I)for(var e=jt,n=jt+K;e<n;e++){var i=Y[e];Ki(i,["style"]),Gi(i,P),Gi(i,W)}Hn(),ee=!0}}function Rn(){var t=In();$.innerHTML!==t&&($.innerHTML=t)}function In(){var t=Pn(),e=t[0]+1,n=t[1]+1;return e===n?e+"":e+" to "+n}function Pn(t){null==t&&(t=ii());var n,i,a,r=jt;if(ct||ut?(rt||ot)&&(i=-(parseFloat(t)+ut),a=i+st+2*ut):rt&&(i=_[jt],a=i+st),rt)_.forEach(function(t,e){e<kt&&((ct||ut)&&t<=i+.5&&(r=e),.5<=a-t&&(n=e))});else{if(ot){var e=ot+lt;ct||ut?(r=Math.floor(i/e),n=Math.ceil(a/e-1)):n=r+Math.ceil(st/e)-1}else if(ct||ut){var o=ft-1;if(ct?(r-=o/2,n=jt+o/2):n=jt+o,ut){var u=ut*ft/st;r-=u,n+=u}r=Math.floor(r),n=Math.ceil(n)}else n=r+ft-1;r=Math.max(r,0),n=Math.min(n,kt-1)}return[r,n]}function zn(){if(St&&!te){var t=Pn();t.push(Ht),Fn.apply(null,t).forEach(function(t){if(!ji(t,xe)){var e={};e[C]=function(t){t.stopPropagation()},ea(t,e),ea(t,be),t.src=Xi(t,"data-src");var n=Xi(t,"data-srcset");n&&(t.srcset=n),Vi(t,"loading")}})}}function Wn(t){Vi(t,"loaded"),qn(t)}function qn(t){Vi(t,xe),Gi(t,"loading"),na(t,be)}function Fn(t,e,n){var i=[];for(n||(n="img");t<=e;)Fi(Y[t].querySelectorAll(n),function(t){i.push(t)}),t++;return i}function jn(){var t=Fn.apply(null,Pn());Hi(function(){Vn(t,Xn)})}function Vn(n,t){return Z?t():(n.forEach(function(t,e){!St&&t.complete&&qn(t),ji(t,xe)&&n.splice(e,1)}),n.length?void Hi(function(){Vn(n,t)}):t())}function Gn(){zn(),Kn(),Rn(),$n(),function(){if(Ct&&(Ie=0<=Re?Re:sn(),Re=-1,Ie!==Pe)){var t=Se[Pe],e=Se[Ie];Yi(t,{tabindex:"-1","aria-label":We+(Pe+1)}),Gi(t,ze),Yi(e,{"aria-label":We+(Ie+1)+qe}),Ki(e,"tabindex"),Vi(e,ze),Pe=Ie}}()}function Qn(t,e){for(var n=[],i=t,a=Math.min(t+e,kt);i<a;i++)n.push(Y[i].offsetHeight);return Math.max.apply(null,n)}function Xn(){var t=gt?Qn(jt,ft):Qn(Dt,K),e=S||V;e.style.height!==t&&(e.style.height=t+"px")}function Yn(){_=[0];var n=F?"left":"top",i=F?"right":"bottom",a=Y[0].getBoundingClientRect()[n];Fi(Y,function(t,e){e&&_.push(t.getBoundingClientRect()[n]-a),e===kt-1&&_.push(t.getBoundingClientRect()[i]-a)})}function Kn(){var t=Pn(),n=t[0],i=t[1];Fi(Y,function(t,e){n<=e&&e<=i?Qi(t,"aria-hidden")&&(Ki(t,["aria-hidden","tabindex"]),Vi(t,ge)):Qi(t,"aria-hidden")||(Yi(t,{"aria-hidden":"true",tabindex:"-1"}),Gi(t,ge))})}function Jn(t){return t.nodeName.toLowerCase()}function Un(t){return"button"===Jn(t)}function _n(t){return"true"===t.getAttribute("aria-disabled")}function Zn(t,e,n){t?e.disabled=n:e.setAttribute("aria-disabled",n.toString())}function $n(){if(xt&&!mt&&!yt){var t=we?Ae.disabled:_n(Ae),e=Me?Ne.disabled:_n(Ne),n=jt<=Gt,i=!mt&&Qt<=jt;n&&!t&&Zn(we,Ae,!0),!n&&t&&Zn(we,Ae,!1),i&&!e&&Zn(Me,Ne,!0),!i&&e&&Zn(Me,Ne,!1)}}function ti(t,e){x&&(t.style[x]=e)}function ei(t){return null==t&&(t=jt),rt?(st-(ut?lt:0)-(_[t+1]-_[t]-lt))/2:ot?(st-ot)/2:(ft-1)/2}function ni(){var t=st+(ut?lt:0)-(ot?(ot+lt)*kt:_[kt]);return ct&&!yt&&(t=ot?-(ot+lt)*(kt-1)-ei():ei(kt-1)-_[kt-1]),0<t&&(t=0),t}function ii(t){var e;if(null==t&&(t=jt),F&&!rt)if(ot)e=-(ot+lt)*t,ct&&(e+=ei());else{var n=d?kt:ft;ct&&(t-=ei()),e=100*-t/n}else e=-_[t],ct&&rt&&(e+=ei());return Rt&&(e=Math.max(e,It)),e+=!F||rt||ot?"px":"%"}function ai(t){ti(G,"0s"),ri(t)}function ri(t){null==t&&(t=ii()),G.style[zt]=Wt+t+qt}function oi(t,e,n,i){var a=t+ft;yt||(a=Math.min(a,kt));for(var r=t;r<a;r++){var o=Y[r];i||(o.style.left=100*(r-jt)/ft+"%"),B&&p&&(o.style[p]=o.style[h]=B*(r-t)/1e3+"s"),Gi(o,e),Vi(o,n),i&&Ot.push(o)}}function ui(t,e){Pt&&nn(),(jt!==Vt||e)&&(_t.emit("indexChanged",Si()),_t.emit("transitionStart",Si()),gt&&jn(),je&&t&&0<=["click","keydown"].indexOf(t.type)&&mi(),Jt=!0,an())}function li(t){return t.toLowerCase().replace(/-/g,"")}function si(t){if(I||Jt){if(_t.emit("transitionEnd",Si(t)),!I&&0<Ot.length)for(var e=0;e<Ot.length;e++){var n=Ot[e];n.style.left="",h&&p&&(n.style[h]="",n.style[p]=""),Gi(n,z),Vi(n,W)}if(!t||!I&&t.target.parentNode===G||t.target===G&&li(t.propertyName)===li(zt)){if(!Pt){var i=jt;nn(),jt!==i&&(_t.emit("indexChanged",Si()),ai())}"inner"===R&&_t.emit("innerLoaded",Si()),Jt=!1,Vt=jt}}}function ci(t,e){if(!ie)if("prev"===t)fi(e,-1);else if("next"===t)fi(e,1);else{if(Jt){if(Xt)return;si()}var n=ln(),i=0;if("first"===t?i=-n:"last"===t?i=I?K-ft-n:K-1-n:("number"!=typeof t&&(t=parseInt(t)),isNaN(t)||(e||(t=Math.max(0,Math.min(K-1,t))),i=t-n)),!I&&i&&Math.abs(i)<ft){var a=0<i?1:-1;i+=Gt<=jt+i-K?K*a:2*K*a*-1}jt+=i,I&&yt&&(jt<Gt&&(jt+=K),Qt<jt&&(jt-=K)),ln(jt)!==ln(Vt)&&ui(e)}}function fi(t,e){if(Jt){if(Xt)return;si()}var n;if(!e){for(var i=bi(t=xi(t));i!==Te&&[Ae,Ne].indexOf(i)<0;)i=i.parentNode;var a=[Ae,Ne].indexOf(i);0<=a&&(n=!0,e=0===a?-1:1)}if(mt){if(jt===Gt&&-1===e)return void ci("last",t);if(jt===Qt&&1===e)return void ci("first",t)}e&&(jt+=dt*e,rt&&(jt=Math.floor(jt)),ui(n||t&&"keydown"===t.type?t:null))}function di(){Fe=setInterval(function(){fi(null,Xe)},Et),je=!0}function vi(){clearInterval(Fe),je=!1}function pi(t,e){Yi(Ye,{"data-action":t}),Ye.innerHTML=Je[0]+t+Je[1]+e}function hi(){di(),Ye&&pi("stop",At[1])}function mi(){vi(),Ye&&pi("start",At[0])}function yi(){je?(mi(),Ge=!0):(hi(),Ge=!1)}function gi(t){t.focus()}function xi(t){return Ci(t=t||m.event)?t.changedTouches[0]:t}function bi(t){return t.target||m.event.srcElement}function Ci(t){return 0<=t.type.indexOf("touch")}function wi(t){t.preventDefault?t.preventDefault():t.returnValue=!1}function Mi(){return a=$e.y-Ze.y,r=$e.x-Ze.x,t=Math.atan2(a,r)*(180/Math.PI),e=Yt,n=!1,i=Math.abs(90-Math.abs(t)),90-e<=i?n="horizontal":i<=e&&(n="vertical"),n===H.axis;var t,e,n,i,a,r}function Ti(t){if(Jt){if(Xt)return;si()}Tt&&je&&vi(),tn=!0,_e&&(Oi(_e),_e=null);var e=xi(t);_t.emit(Ci(t)?"touchStart":"dragStart",Si(t)),!Ci(t)&&0<=["img","a"].indexOf(Jn(bi(t)))&&wi(t),$e.x=Ze.x=e.clientX,$e.y=Ze.y=e.clientY,I&&(Ue=parseFloat(G.style[zt].replace(Wt,"")),ti(G,"0s"))}function Ei(t){if(tn){var e=xi(t);$e.x=e.clientX,$e.y=e.clientY,I?_e||(_e=Hi(function(){!function t(e){if(!Kt)return void(tn=!1);Oi(_e);tn&&(_e=Hi(function(){t(e)}));"?"===Kt&&(Kt=Mi());if(Kt){!Ce&&Ci(e)&&(Ce=!0);try{e.type&&_t.emit(Ci(e)?"touchMove":"dragMove",Si(e))}catch(t){}var n=Ue,i=en($e,Ze);if(!F||ot||rt)n+=i,n+="px";else{var a=d?i*ft*100/((st+lt)*kt):100*i/(st+lt);n+=a,n+="%"}G.style[zt]=Wt+n+qt}}(t)})):("?"===Kt&&(Kt=Mi()),Kt&&(Ce=!0)),("boolean"!=typeof t.cancelable||t.cancelable)&&Ce&&t.preventDefault()}}function Ai(i){if(tn){_e&&(Oi(_e),_e=null),I&&ti(G,""),tn=!1;var t=xi(i);$e.x=t.clientX,$e.y=t.clientY;var a=en($e,Ze);if(Math.abs(a)){if(!Ci(i)){var n=bi(i);ea(n,{click:function t(e){wi(e),na(n,{click:t})}})}I?_e=Hi(function(){if(F&&!rt){var t=-a*ft/(st+lt);t=0<a?Math.floor(t):Math.ceil(t),jt+=t}else{var e=-(Ue+a);if(e<=0)jt=Gt;else if(e>=_[kt-1])jt=Qt;else for(var n=0;n<kt&&e>=_[n];)e>_[jt=n]&&a<0&&(jt+=1),n++}ui(i,a),_t.emit(Ci(i)?"touchEnd":"dragEnd",Si(i))}):Kt&&fi(i,0<a?-1:1)}}"auto"===H.preventScrollOnTouch&&(Ce=!1),Yt&&(Kt="?"),Tt&&!je&&di()}function Ni(){(S||V).style.height=_[jt+ft]-_[jt]+"px"}function Li(){var t=ot?(ot+lt)*K/st:K/ft;return Math.min(Math.ceil(t),K)}function Bi(){if(Ct&&!pe&&De!==ke){var t=ke,e=De,n=_i;for(De<ke&&(t=De,e=ke,n=Ui);t<e;)n(Se[t]),t++;ke=De}}function Si(t){return{container:G,slideItems:Y,navContainer:He,navItems:Se,controlsContainer:Te,hasControls:de,prevButton:Ae,nextButton:Ne,items:ft,slideBy:dt,cloneCount:Dt,slideCount:K,slideCountNew:kt,index:jt,indexCached:Vt,displayIndex:on(),navCurrentIndex:Ie,navCurrentIndexCached:Pe,pages:De,pagesCached:ke,sheet:Bt,isOn:U,event:t||{}}}M&&console.warn("No slides found in",H.container)};return aa}();

(function () {

  /**
   * Colsone Error Fix
   * ===
   * Avoid `console` errors in browsers that lack a console.
   */

  var method;
  var noop = function () {};
  var methods = [
    'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
    'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
    'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
    'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
  ];
  var length = methods.length;
  var console = ( window.console = window.console || {} );

  while ( length-- ) {
    method = methods[length];

    // Only stub undefined methods.
    if ( ! console[method] ) {
      console[method] = noop;
    }
  }
}());

(function () {

  /**
   * Skip Link Focus Fix
   * ===
   * Helps with accessibility for keyboard only users.
   * 
   * Fixes a bug in IE <= 11 where using the tab key to
   * use an anchor link breaks the tab flow of the documnet.
   * 
   * Learn more: https://git.io/vWdr2
   */

  var isIe = /(trident|msie)/i.test( navigator.userAgent );

  if ( isIe && document.getElementById && window.addEventListener ) {
    window.addEventListener( 'hashchange', function() {
      var id = location.hash.substring( 1 ),
        element;

      if ( ! ( /^[A-z0-9_-]+$/.test( id ) ) ) {
        return;
      }

      element = document.getElementById( id );

      if ( element ) {
        if ( ! ( /^(?:a|select|input|button|textarea)$/i.test( element.tagName ) ) ) {
          element.tabIndex = -1;
        }

        element.focus();
      }
    }, false );
  }
})();

(function ($) {
  'use strict';

  /**
   * WP Aria Navigations
   * ---
   * Use in conjunction with WP Aria Menu Walker for a
   * more accessible navigation. Heavily uses timers to
   * gage mouse intent.
   * 
   * Features:
   * - Toggle 'aria-expanded' attribute on focus and hover
   * - Display sub-menu on tab focus
   * - Fixed Sub-Menu - Sub-menu displays fixed on 'moutseout' or
   *   until user clicks out
   * - Hover Intent like feature - Don't enable the fixed sub-menu
   *   until mouse stays on menu item for extended time
   * - Optional Overlay - Set the overlay paramater to true to enable
   *
   * Todo:
   * - Inline CSS
   */

  $.fn.wpAriaNav = function ( options ) {
    options = jQuery.extend({
      overlay         : true,
      overlayClass    : 'js-aria-hover-overlay',
      menuHoverClass  : 'is-active',
      mouseOutTimeout : 0,
      mouseDelay      : 350,
    }, options );

    var $aria_nav         = $(this);
    var $nav_top_item     = $aria_nav.find('> ul > li > a');
    var $aria_dropdown    = $aria_nav.find('li[aria-haspopup="true"]');
    var $aria_nav_overlay = '';
    var has_overlay       = false;

    var unset_active_delay = 0;
    var mouseleave_timer;
    var delay_timer;
    var first_leave_timer;
    var is_first_leave = false;

    if ( options.overlayClass !== '' && options.overlay === true ) {
      has_overlay = true;
    }


    /**
     * Close first and last sub-navs when tabbing out of nav
     */

    var $first_focusable_link = $aria_nav.find('> ul > li:first-child > a');
    var $last_focusable_link  = $aria_nav.find('> ul > li:last-child > a');

    var $last_nav_ul = $aria_nav.find('> ul > li:last-child').children( 'ul' );
    var $next_nav_ul = $last_nav_ul;

    // Loop through all child ULs until we get the last one
    if ( $last_nav_ul.length ) {
      while ( $next_nav_ul.length ) {
        $last_nav_ul = $next_nav_ul;
        $next_nav_ul = $next_nav_ul.children( 'li:last-child' ).children( 'ul' );
      }

      $last_focusable_link = $last_nav_ul.find( 'li:last-child > a' );
    }

    // Close sub-nav when tabbing off of first item
    $first_focusable_link.on( 'keydown', function ( e ) {
      if ( e.which === 9 && e.shiftKey ) {
        aria_unset_active( $aria_dropdown );
      }
    });

    // Close sub-nav when tabbing off of last item
    $last_focusable_link.on( 'keydown', function ( e ) {
      if ( e.which === 9 && ! e.shiftKey ) {
        aria_unset_active( $aria_dropdown );
      }
    });


    /**
     * Add overlay if it's enabled
     */

    if ( has_overlay ) {
      $aria_nav.after('<div class="' + options.overlayClass + '"></div>');
      $aria_nav_overlay = $aria_nav.next( '.' + options.overlayClass );

      // Unset active when overlay is clicked
      $aria_nav_overlay.on('click', function () {
        aria_unset_active( $aria_dropdown );
      });
    }


    // Unset all active Aria items and activate the current
    $aria_dropdown.on('focusin mouseenter', function ( e ) {
      var $this = $(this);
      var $focused_link = $( ':focus' );
      var mouse_delay = options.mouseDelay;

      // Unset active if another item that does not have a child is focused
      if ( ! $focused_link.closest( '.sub-menu .sub-menu' ).length &&
           ! $focused_link.parent().hasClass( 'menu-item-has-children' )) {
        aria_unset_active( $aria_dropdown );
      }


      // Reset our timeout when mouse re-enters after
      // triggering a mouseleave
      clearTimeout( mouseleave_timer );

      // Enable active state
      $this.attr( 'aria-expanded', 'true' )
        .addClass( options.menuHoverClass );

      if ( has_overlay ) {
        $aria_nav_overlay.addClass( options.menuHoverClass );
      }


      // Prevent immediate close if we re-enter the menu quickly
      if ( is_first_leave === true ) {
        mouse_delay = 0;
      }


      /**
       * Ignore mouse if it's 'just passing through'
       * ---
       * `unset_active_delay` default is 0 and is only updated if
       * the mouse is sticking around
       */

      delay_timer = setTimeout(function () {
        unset_active_delay = options.mouseOutTimeout;
      }, mouse_delay );

      e.preventDefault();
    });


    // Unset all active items
    $nav_top_item.on('focusin mouseenter', function (e) {
      aria_unset_active( $aria_dropdown );

      e.preventDefault();
    });


    // Don't close dropdowns right away when mouse leaves
    $aria_dropdown.on('mouseleave', function () {

      /**
       * Delay sub-nav closing
       */

      mouseleave_timer = setTimeout(function () {
        aria_unset_active( $aria_dropdown );
      }, unset_active_delay );


      /**
       * Reset 'Just passing through' Timer
       */

      unset_active_delay = 0;
      clearTimeout( delay_timer );


      /**
       * Track if mouse re-enters quickly
       * ---
       * Bypasses the 'Just passing through' timer to allow
       * sub-nav to stick around if mouse accidentally leaves
       * and comes back quickly
       */

      is_first_leave = true;

      clearTimeout( first_leave_timer );
      first_leave_timer = setTimeout(function () {
        is_first_leave = false;
      }, options.mouseDelay );

    });


    // Change Aria status and remove active class
    function aria_unset_active( $aria_dropdown ) {
      $aria_dropdown.attr( 'aria-expanded', 'false' )
        .removeClass( options.menuHoverClass );

      if ( has_overlay ) {
        $aria_nav_overlay.removeClass( options.menuHoverClass );
      }
    }
  };

})(jQuery);

(function ($) {
  'use strict';

  /**
   * Accessibility Enhancements
   * ===
   */


  /**
   * Aria Nav Controls
   * ---
   * Remove or exclude the 'js-accessible-menu' class if the
   * nav does not have sub-items.
   */

  var $aria_nav = $('.js-accessible-menu');

  $aria_nav.each(function () {
    $(this).wpAriaNav({
      overlay: false
    });
  });



  /**
   * Force all article external links to open in new tabs
   * ---
   * Applies only to anchor tags that don't already
   * have a target attribute
   */

  // Ignore article links that already have a target set,
  // link to an anchor, or link to a relative URL
  var $page_links = $( '.entry-content, .page-content' ).find( 'a:not([target]):not([href^="#"]):not([href^="/"])' );

  // Get the site's base URL
  var pathArray = location.href.split( '/' );
  var host      = pathArray[2]; // ex. 'www.imagebox.com', 'domain.com'

  $page_links.each(function () {
    var $this = $(this);
    var this_href = $this.attr('href');
    var this_rel  = $this.attr('rel');

    if ( typeof this_href !== typeof undefined && this_href !== false ) {
      // Check if link is external
      if ( this_href.indexOf( host ) === -1 ) {
        $this.attr( 'target', '_blank' );

        // Check if rel is already set
        if ( typeof this_rel !== typeof undefined && this_rel !== false ) {
          $this.attr( 'rel', 'nofollow noopener' );
        }
      }
    }
  });


  /**
   * Add `focusable="false"` to SVGs inside links or buttons
   */

  var $focusable_svgs = $('a svg, button svg');

  $focusable_svgs.each(function () {
    var $this = $(this);
    var this_focusable = $this.attr('focusable');

    if ( typeof this_focusable !== typeof undefined && this_focusable !== false ) {
      $this.attr( 'focusable', 'false' );
    }
  });

})(jQuery);

(function ($) {
  'use strict';

  /**
   * Site Carousels
   * ===
   */

  /**
   * Hero Carousel
   * ---
   * Tiny Slider
   * [https://github.com/ganlanyuan/tiny-slider#options]
   */

  var sliders = document.querySelectorAll( '.js-carousel' );
  var slider_init;

  if ( sliders.length ) {
    sliders.forEach(function (slider) {
      slider_init = tns({
        container: slider,
        mouseDrag: true,
        speed: 800,
        autoplay: true,
        controlsText: [
          '<span class="vh">Previous</span><svg class="svg-left-icon" width="40" height="40" focusable="false"><use href="#arrow-left-icon"/></svg>',
          '<span class="vh">Next</span><svg class="svg-right-icon" width="40" height="40" focusable="false"><use href="#arrow-right-icon"/></svg>',
        ]
      });
    });
  }

})(jQuery);

(function ($) {
  'use strict';

  /**
   * Mobile Header Navigation
   * ===
   */


  /**
   * Nav Tray Controls
   * ---
   */

  // Mobile Navigation Toggle
  var $site_wrap = $('body');
  var $mobile_nav_tray = $('.mobile-nav-tray');
  var $mobile_nav_tray_links = $mobile_nav_tray.find('a, input[type="search"], button');
  var $mobile_nav_toggle = $('.js-toggle-nav');

  var $nav_focusable_inputs = $mobile_nav_tray_links.not('[disabled]');
  var $nav_first_input  = $nav_focusable_inputs.first();
  var $nav_last_input   = $nav_focusable_inputs.last();

  // Disable links by default
  $mobile_nav_tray_links.attr( 'tabindex', '-1' );

  $mobile_nav_toggle.on('click', function () {
    toggleNav();
  });

  function toggleNav() {
    $site_wrap.toggleClass( 'show-nav' );

    if ( $site_wrap.hasClass( 'show-nav' )) {
      // Expand tray
      $mobile_nav_toggle.attr( 'aria-expanded', 'true' );
      $mobile_nav_tray.attr( 'aria-hidden', 'false' );
      $mobile_nav_tray_links.attr( 'tabindex', 0 );

      // Lock tabbing to links inside tray
      $nav_focusable_inputs = $mobile_nav_tray_links.not('[disabled]');
      $nav_last_input.on( 'keydown', nav_last_focus_lock );
      $nav_first_input.on( 'keydown', nav_first_focus_lock );
    } else {
      // Collapse tray
      $mobile_nav_toggle.attr( 'aria-expanded', 'false' );
      $mobile_nav_tray.attr( 'aria-hidden', 'true' );
      $mobile_nav_tray_links.attr( 'tabindex', '-1' );

      // Unlock tabbing
      $nav_last_input.off( 'keydown', nav_last_focus_lock );
      $nav_first_input.off( 'keydown', nav_first_focus_lock );
    }
  }

  // Hide nav/contact if open and window is resized above 760
  $(window).resize(function () {
    if ( $(window).width() > 760 ) {
      // Collapse tray
      $site_wrap.removeClass('show-nav');
      $mobile_nav_toggle.attr( 'aria-expanded', 'false' );
      $mobile_nav_tray.attr( 'aria-hidden', 'true' );
      $mobile_nav_tray_links.attr( 'tabindex', '-1' );

      // Unlock tabbing
      $nav_last_input.off( 'keydown', nav_last_focus_lock );
      $nav_first_input.off( 'keydown', nav_first_focus_lock );
    }
  });

  // Loop to last input when shift + tabbing
  function nav_first_focus_lock( e ) {
    if ( e.which === 9 && e.shiftKey ) {
      $nav_last_input.focus();
      return false;
    }
  }

  // Loop to first input when tabbing off last input
  function nav_last_focus_lock( e ) {
    if ( e.which === 9 && ! e.shiftKey ) {
      $nav_first_input.focus();
      return false;
    }
  }



  /**
   * Sub Menu Controls
   * ---
   * Click a top level item to display the sub-menu
   */

  var $mobile_nav = $('.navigation--mobile');
  var $active_mobile_nav_item = $mobile_nav.find( '.current-page-ancestor > a' );

  // Set initial active state
  $active_mobile_nav_item
    .addClass( 'is-active' )
    .next( '.sub-menu' )
    .css( 'display', 'block' );

  // Set initial aria attrs
  $active_mobile_nav_item
    .parent( '.current-page-ancestor' )
    .attr( 'aria-expanded', 'true' );

  // Delegate the click event to bind the changing ':not(.is-active)' selector
  $mobile_nav.on('click', '.menu-item-has-children > a:not(.is-active)', function () {
    var $this_link    = $(this);
    var $this_parent  = $this_link.parent();
    var $sub_menu     = $this_link.siblings('.sub-menu');

    var $sibling_parents = $this_link.closest('li')
      .siblings('.menu-item-has-children');

    var $sibling_links = $sibling_parents.find('> a');

    var $sibling_sub_menus = $this_link.closest('li')
      .siblings('.menu-item-has-children')
      .find('> .sub-menu');

    $sibling_links.removeClass('is-active');
    $this_link.addClass('is-active');

    $sibling_parents.attr( 'aria-expanded', 'false' );
    $this_parent.attr( 'aria-expanded', 'true' );

    $sibling_sub_menus.slideUp('800');
    $sub_menu.slideDown('800');

    return false;
  });

})(jQuery);

(function ($) {
  'use strict';

  /**
   * Navigation Search Toggle
   */
  
  var $nav_search_toggle    = $('.js-search-toggle');
  var $nav_search_submit  = $('.nav-search-dropdown .search-submit');

  // Search toggle button
  $nav_search_toggle.on('click', function () {
    var $this = $(this);
    var $this_parent    = $this.parent( '.nav-item-search' );
    var $this_dropdown  = $this.next( '.nav-search-dropdown' );

    // Track show / hide state on parent
    $this_parent.toggleClass('is-active');

    if ( $this_parent.hasClass( 'is-active' )) {
      // Show Dropdown
      show_search_dropdown( $this, $this_dropdown );

    } else {
      // Hide Dropdown
      hide_search_dropdown( $this, $this_dropdown );
    }

    return false;
  });


  /**
   * Hide on 'esc' keypress
   */

  $(document).keydown( function (e) {
    if ( e.keyCode === 27 && $nav_search_toggle.length ) {
      var $button_parent    = $nav_search_toggle.parent( '.nav-item-search' );
      var $button_dropdown  = $nav_search_toggle.next( '.nav-search-dropdown' );

      hide_search_dropdown( $nav_search_toggle, $button_dropdown );
      $button_parent.removeClass( 'is-active' );
    }
  });


  /**
   * Hide on tab out
   */
  
  $nav_search_submit.on('keydown', function ( e ) {
    if ( e.which === 9 && ! e.shiftKey ) {
      var $button_parent    = $nav_search_toggle.parent( '.nav-item-search' );
      var $button_dropdown  = $nav_search_toggle.next( '.nav-search-dropdown' );

      hide_search_dropdown( $nav_search_toggle, $button_dropdown );
      $button_parent.removeClass( 'is-active' );
    }
  });

  $nav_search_toggle.on('keydown', function ( e ) {
    if ( e.which === 9 && e.shiftKey ) {
      var $this = $(this);
      var $button_parent    = $this.parent( '.nav-item-search' );
      var $button_dropdown  = $this.next( '.nav-search-dropdown' );

      hide_search_dropdown( $nav_search_toggle, $button_dropdown );
      $button_parent.removeClass( 'is-active' );
    }
  });


  /**
   * Show Search Dropdown
   */
  function show_search_dropdown( $toggle_button, $dropdown ) {
    $toggle_button.attr( 'aria-expanded', 'true' );
    $dropdown.css( 'display', 'block' );
    $dropdown.find( 'input[type="search"]' ).focus();
  }


  /**
   * Hide Search Dropdown
   */
  function hide_search_dropdown( $toggle_button, $dropdown ) {
    $toggle_button.attr( 'aria-expanded', 'false' );
    $dropdown.css( 'display', 'none' );
  }

})(jQuery);

(function ($) {
  'use strict';

  /**
   * Site javascripts
   * ===
   */

  // Wrap video embeds in Flexible Container
  $('iframe[src*="youtube.com"]:not(.not-responsive), iframe[src*="vimeo.com"]:not(.not-responsive)')
    .attr( 'frameborder', 0 )
    .wrap('<div class="flexible-container"></div>');

  // Resize Maps
  $('iframe[src*="google.com/map"]:not(.not-responsive)')
    .attr( 'frameborder', 0 )
    .wrap('<div class="flexible-container"></div>');


        /**
     * Fancy Number Ticker
     */
    $.fn.isInViewport = function() {
      var elementTop = $(this).offset().top;
      var elementBottom = elementTop + $(this).outerHeight();

      var viewportTop = $(window).scrollTop();
      var viewportBottom = viewportTop + $(window).height();

      return elementBottom > viewportTop && elementTop < viewportBottom;
    };

    if ( $('.odometer').length ) {
      $(window).on('resize scroll', function () {
        $('.odometer').each( function () {
          var $this = $(this);
          var end_number = $this.data( 'animate-number' );

          if ( $this.isInViewport() ) {
            setTimeout(function () {
              $this.html( end_number );
            }, 250 );
          }
        });
      }).resize();
    }


    $('.open-bio').magnificPopup({type: 'inline'});
    // homepage new slide


})(jQuery);

(function ($) {
  'use strict';

  /**
   * Social Share Popups
   * ===
   */

  function windowPopup( url, width, height ) {
    // Calculate the position of the popup so
    // it’s centered on the screen.
    var pos_left  = (screen.width / 2) - (width / 2);
    var pos_top   = (screen.height / 2) - (height / 2);
    var popup_url = 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,width=' + width + ',height=' + height + ',top=' + pos_top + ',left=' + pos_left;

    window.open( url, '', popup_url );
  }

  $('.js-social-share').on('click', function () {
    windowPopup( $(this).attr('href'), 500, 300 );
    return false;
  });
  
})(jQuery);

(function ($) {
  'use strict';

  /**
   * Sticky Header
   * ===
   */
  
  var $site_header = $('#masthead');

  // Init Sticky Nav
  toggleStickyNav( $site_header );

  // Re-calculate sticky on scroll
  $(window).on( 'scroll', function () {
    toggleStickyNav( $site_header );
  });

  // Re-calculate sticky on window resize
  $(window).on( 'resize', function () {
    toggleStickyNav( $site_header );
  });


  /**
   * Toggle Sticky Class
   * ---
   */

  function toggleStickyNav( $el ) {
    if ( $el.is( ':visible' ) && $(window).scrollTop() > 40 ) {
      $el.addClass('minify-header');
    } else {
      $el.removeClass('minify-header');
    }
  }

})(jQuery);
