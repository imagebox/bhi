(function ($) {
  'use strict';

  /**
   * Site Carousels
   * ===
   */

  /**
   * Hero Carousel
   * ---
   * Tiny Slider
   * [https://github.com/ganlanyuan/tiny-slider#options]
   */

  var sliders = document.querySelectorAll( '.js-carousel' );
  var slider_init;

  if ( sliders.length ) {
    sliders.forEach(function (slider) {
      slider_init = tns({
        container: slider,
        mouseDrag: true,
        speed: 800,
        autoplay: true,
        controlsText: [
          '<span class="vh">Previous</span><svg class="svg-left-icon" width="40" height="40" focusable="false"><use href="#arrow-left-icon"/></svg>',
          '<span class="vh">Next</span><svg class="svg-right-icon" width="40" height="40" focusable="false"><use href="#arrow-right-icon"/></svg>',
        ]
      });
    });
  }

})(jQuery);
