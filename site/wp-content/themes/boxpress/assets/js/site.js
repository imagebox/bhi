(function ($) {
  'use strict';

  /**
   * Site javascripts
   * ===
   */

  // Wrap video embeds in Flexible Container
  $('iframe[src*="youtube.com"]:not(.not-responsive), iframe[src*="vimeo.com"]:not(.not-responsive)')
    .attr( 'frameborder', 0 )
    .wrap('<div class="flexible-container"></div>');

  // Resize Maps
  $('iframe[src*="google.com/map"]:not(.not-responsive)')
    .attr( 'frameborder', 0 )
    .wrap('<div class="flexible-container"></div>');


        /**
     * Fancy Number Ticker
     */
    $.fn.isInViewport = function() {
      var elementTop = $(this).offset().top;
      var elementBottom = elementTop + $(this).outerHeight();

      var viewportTop = $(window).scrollTop();
      var viewportBottom = viewportTop + $(window).height();

      return elementBottom > viewportTop && elementTop < viewportBottom;
    };

    if ( $('.odometer').length ) {
      $(window).on('resize scroll', function () {
        $('.odometer').each( function () {
          var $this = $(this);
          var end_number = $this.data( 'animate-number' );

          if ( $this.isInViewport() ) {
            setTimeout(function () {
              $this.html( end_number );
            }, 250 );
          }
        });
      }).resize();
    }


    $('.open-bio').magnificPopup({type: 'inline'});
    // homepage new slide


})(jQuery);
