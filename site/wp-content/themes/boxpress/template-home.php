<?php
/**
 * Template Name: Homepage
 *
 * Page template to display the homepage.
 *
 * @package boxpress
 */
get_header(); ?>

  <article class="homepage">

    <?php
    $alert_text = get_field('alert_text');
    $alert_link = get_field('alert_link');
    ?>

    <?php if ( $alert_text ) : ?>
      <nav class="homepage-alert-box home-display">
         <div class="wrap">
           <div class="alert-box-text">
             <p><?php echo $alert_text; ?></p>
             <?php if ( $alert_link ) : ?>
               <a
                 href="<?php echo esc_url( $alert_link['url'] ); ?>"
                 target="<?php echo esc_attr( $alert_link['target'] ); ?>">
                 <?php echo $alert_link['title']; ?>
               </a>
             <?php endif; ?>
           </div>
         </div>
      </nav>
    <?php endif; ?>



    <?php // Hero ?>
    <?php get_template_part( 'template-parts/homepage/homepage-hero' ); ?>
    <?php get_template_part( 'template-parts/homepage/top-shortcut-links-new' ); ?>
    <?php get_template_part( 'template-parts/homepage/middle-stat-callouts' ); ?>
    <?php get_template_part( 'template-parts/homepage/map-pins' ); ?>
    <?php get_template_part( 'template-parts/homepage/callout-split' ); ?>
    <?php get_template_part( 'template-parts/homepage/bottom-cpt' ); ?>
    <?php get_template_part( 'template-parts/homepage/partner-callout' ); ?>

  </article>

<?php get_footer(); ?>
