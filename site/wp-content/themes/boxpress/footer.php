<?php
/**
 * Template footer
 *
 * Contains the closing of the main el and all content after.
 *
 * @package boxpress
 */
?>
</main>




<footer id="colophon" class="site-footer section footer-new" role="contentinfo">
 <div class="wrap">
   <div class="primary-footer-new">
     <div class="footer-logo-new">
       <div class="site-branding">
         <a href="<?php echo esc_url( home_url( '/' )); ?>" rel="home">
           <span class="vh"><?php bloginfo('name'); ?></span>
           <svg class="site-logo" width="261" height="75" focusable="false">
             <use href="#site-logo-footer"/>
           </svg>
         </a>
       </div>
       <div class="footer-addy-new">
         <?php // Utility Navigation ?>
              <?php if ( has_nav_menu( 'footer' )) : ?>
                <h5>Quick Links</h5>
                <nav class="navigation--utility"
                  aria-label="<?php _e( 'Utility Navigation', 'boxpress' ); ?>"
                  role="navigation">
                  <ul class="nav-list">
                    <?php
                      wp_nav_menu( array(
                        'theme_location'  => 'footer',
                        'items_wrap'      => '%3$s',
                        'container'       => false,
                        'walker'          => new Aria_Walker_Nav_Menu(),
                      ));
                    ?>
                  </ul>
                </nav>
              <?php endif; ?>
       </div>
       <div class="footer-link-new">
           <h5>Contact</h5>
          <?php get_template_part( 'template-parts/global/address-block' ); ?>
       </div>
     </div>
     <div class="footer-social-new">
      <?php get_template_part( 'template-parts/global/social-nav' ); ?>
     </div>
   </div>
   <div class="secondary-footer">
     <div class="footer-copy-new">
       <p>
                <small>
                  <?php _e('Copyright', 'boxpress'); ?> &copy; <?php echo date('Y'); ?>
                  <?php
                    $company_name     = get_bloginfo( 'name', 'display' );
                    $alt_company_name = get_field( 'alternative_company_name', 'option' );

                    if ( ! empty( $alt_company_name )) {
                      $company_name = $alt_company_name;
                    }
                  ?>
                  <?php echo $company_name; ?>.
                  <?php _e('All rights reserved.', 'boxpress'); ?>
                  <p>Basic Health International is a tax exempt charitable organization under section 501(c) (3) of the Internal Revenue Code of 1986, as amended.</p>
                </small>
              </p>
     </div>
     <div class="footer-copy-imagebox">
       <div class="imagebox">
         <p>
           <small>
             <?php _e('Website by', 'boxpress'); ?>
             <a href="https://imagebox.com" target="_blank">
               <span class="vh">Imagebox</span>
               <svg class="imagebox-logo-svg" width="81" height="23" focusable="false">
                 <use href="#imagebox-logo"/>
               </svg>
             </a>
           </small>
         </p>
       </div>
     </div>
 </div>
  </div>

</footer>


</div>

<?php wp_footer(); ?>

<?php // Footer Tracking Codes ?>
<?php the_field( 'footer_tracking_codes', 'option' ); ?>

</body>
</html>
