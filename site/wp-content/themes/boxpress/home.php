<?php
/**
 * The template for displaying the Blog page.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package boxpress
 */

 get_header(); ?>

 <?php require_once('template-parts/banners/banner--highlight.php'); ?>

<section class="section resource-grid">
    <div class="wrap">
      <div class="l-sidebar">
        <div class="l-main-col">
          <?php if ( have_posts() ) : ?>
            <div class="l-grid l-grid--three-col">
              <?php while ( have_posts() ) : the_post(); ?>
                <div class="l-grid-item">
                  <div class="card-shadow">
                    <a href="<?php the_permalink(); ?>">
                    <div class="thumbnail">
                      <?php the_post_thumbnail('blog_thumb');?>
                    </div>

                    <div class="card-body">
                    <div class="card--body">
                      <span><?php the_date('F, Y'); ?></span>
                      <h3><?php the_title(); ?></h3>
                      <p class="card-excerpt"><?php echo wp_trim_words( strip_shortcodes (get_the_content()), 20, '&hellip;' ); ?></p>
                    </div>
                    <div class="card-footer">
                      <div class="button button--text" href="<?php the_permalink(); ?>">Learn More</div>
                    </div>
                  </div>
                </a>
                </div>
              </div>
              <?php endwhile; ?>
            </div>
            <?php boxpress_pagination(); ?>
          <?php else : ?>

            <?php get_template_part( 'template-parts/content/content', 'none' ); ?>

          <?php endif; ?>

        </div>
        <div class="l-aside-col">
          <?php get_sidebar('resources'); ?>
        </div>
      </div>
    </div>
  </section>

<?php get_footer(); ?>
