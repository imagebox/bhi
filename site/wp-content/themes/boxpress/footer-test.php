<footer id="colophon" class="site-footer" role="contentinfo">

    <div class="primary-footer">
      <div class="wrap">
        <div class="footer-col-left">
          <div class="site-branding">
            <a href="<?php echo esc_url( home_url( '/' )); ?>" rel="home">
              <span class="vh"><?php bloginfo('name'); ?></span>
              <svg class="site-logo" width="63" height="73" focusable="false">
                <use href="#site-logo"/>
              </svg>
            </a>
          </div>
          <div class="site-copyright">
            <p>
              <small>
                <?php _e('Copyright', 'boxpress'); ?> &copy; <?php echo date('Y'); ?>
                <?php
                  $company_name     = get_bloginfo( 'name', 'display' );
                  $alt_company_name = get_field( 'alternative_company_name', 'option' );

                  if ( ! empty( $alt_company_name )) {
                    $company_name = $alt_company_name;
                  }
                ?>
                <?php echo $company_name; ?>.
                <?php _e('All rights reserved.', 'boxpress'); ?>
              </small>
            </p>
          </div>
        </div>
        <div class="footer-col-right">
            <?php get_template_part( 'template-parts/global/social-nav' ); ?>
          <div class="imagebox">
            <p>
              <small>
                <?php _e('Website by', 'boxpress'); ?>
                <a href="https://imagebox.com" target="_blank">
                  <span class="vh">Imagebox</span>
                  <svg class="imagebox-logo-svg" width="81" height="23" focusable="false">
                    <use href="#imagebox-logo"/>
                  </svg>
                </a>
              </small>
            </p>
          </div>
        </div>
      </div>
    </div>

</footer>
