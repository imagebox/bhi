<?php
/**
 * The template for displaying archive pages.
 *
 * Template Name: Press
 *
 * @package boxpress
 */


get_header(); ?>

<?php require_once('template-parts/banners/banner--press.php'); ?>

<section class="section resource-grid">
  <div class="wrap">
    <div class="l-sidebar">
      <div class="l-main-col">
        <?php
        // display press news

          $press_query_args = array(
            'post_type' => 'press',
            'posts_per_page' => -1,
             'order'       => 'ASC'
          );
          $press_query = new $wp_query( $press_query_args );
        ?>

      <?php if ( $press_query->have_posts() ) : ?>

       <div class="l-grid l-grid--three-col">
        <?php while ( $press_query->have_posts() ) : $press_query->the_post(); ?>

          <div class="l-grid-item">
           <?php
           $press_link = get_field('press_link');
           $press_watch_link = get_field('press_watch_link');
           $press_text = get_field('press_text');
           ?>

            <div class="card-shadow">
              <div class="thumbnail">
                <?php the_post_thumbnail('blog-thumb');?>
              </div>
              <div class="card-body">
                <div class="card--body">
                  <span><?php echo $press_text; ?></span>
                  <h3><?php echo wp_trim_words( strip_shortcodes (get_the_title()), 20, '&hellip;' ); ?></h3>
                  <p><?php echo wp_trim_words( strip_shortcodes (get_the_content()), 20, '&hellip;' ); ?></p>
                </div>
                <div class="card-footer">
                  <?php if ( $press_link ) : ?>
                  <a class="button button--text"
                    href="<?php echo esc_url( $press_link['url'] ); ?>"
                    target="<?php echo esc_attr( $press_link['target'] ); ?>">
                    Learn More
                  </a>
                  <?php endif; ?>

                  <?php if ( $press_watch_link ) : ?>
                  <a class="button button--text"
                    href="<?php echo esc_url( $press_watch_link['url'] ); ?>"
                    target="<?php echo esc_attr( $press_watch_link['target'] ); ?>">
                    Watch it here
                  </a>
                  <?php endif; ?>
                </div>
              </div>
            </div>



          </div>

          <?php endwhile; ?>
       </div>
       <?php endif; ?>

      </div>
      <div class="l-aside-col">
        <?php get_sidebar('resources'); ?>
      </div>

    </div>
  </div>
</section>


<?php get_footer(); ?>
