<?php
/**
 * Template part for displaying blog posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package boxpress
 */
?>

<?php

$paper_author = get_field('paper_author');
$paper_pdf = get_field('paper_pdf');
$paper_content = get_field('paper_content');

 ?>

<article class="paper-row">
  <header class="entry-header">
    <h2 class="entry-title"><?php the_title(); ?></h2>
  </header>

  <div class="entry-content">
    <div class="paper-body">
      <?php if ( $paper_author ) :  ?>
        <?php echo $paper_author; ?>
      <?php endif; ?>

      <?php if ( $paper_content ) :  ?>
        <?php echo $paper_content; ?>
      <?php endif; ?>

      <?php if ( $paper_pdf ) :  ?>
       <a class="button pdf--button"
         href="<?php echo esc_url( $paper_pdf['url'] ); ?>"
         target="_blank"> PDF Version
       </a>
     <?php endif; ?>
    </div>
  </div>
</article>
