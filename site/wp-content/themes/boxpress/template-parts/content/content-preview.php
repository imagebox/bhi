<?php
/**
 * Displays the preview content block
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package boxpress
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class( 'content--preview' ); ?>>
  <header class="entry-header">
    <h2 class="entry-title">
      <a href="<?php the_permalink(); ?>">
        <?php the_title(); ?>
      </a>
    </h2>

    <div class="entry-meta">
      <?php boxpress_posted_on(); ?>
    </div>
  </header>

  <div class="entry-summary">
    <?php the_excerpt(); ?>
  </div>

  <footer class="entry-footer">
    <a class="text-button" href="<?php the_permalink(); ?>"><?php _e('Learn More', 'boxpress'); ?></a>
  </footer>
</article>
