<?php
/**
 * Template part for displaying blog posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package boxpress
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class( 'content--single' ); ?>>
  <header class="entry-header">
    <h3 class="entry-title"><?php the_title(); ?></h3>



    <div class="entry-meta">
      <?php boxpress_posted_on(); ?>
    </div>
  </header>

  <div class="entry-content">
    <?php the_content(); ?>
  </div>


</article>
