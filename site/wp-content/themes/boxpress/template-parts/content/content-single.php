<?php
/**
 * Template part for displaying blog posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package boxpress
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class( 'content--single' ); ?>>
  <header class="entry-header">
    <h1 class="entry-title"><?php the_title(); ?></h1>

    <?php if ( has_post_thumbnail() ) : ?>
      <?php the_post_thumbnail('home_index_thumb'); ?>
    <?php endif; ?>

    <div class="entry-meta">
      <?php boxpress_posted_on(); ?>
    </div>
  </header>

  <div class="entry-content">
    <?php the_content(); ?>
  </div>

  <footer class="entry-footer">
    <?php include( get_template_directory() . '/template-parts/social-share.php'); ?>
  </footer>
</article>
