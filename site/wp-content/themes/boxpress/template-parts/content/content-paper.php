<?php
/**
 * Template part for displaying blog posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package boxpress
 */
?>








<div id="popup-<?php echo get_the_ID(); ?>" class="mfp-hide popup">
  <div class="wrap modal-wrap">
    <div class="modal-content">
      <div class="modal-header">
        <button title="Close (Esc)" type="button" class="mfp-close">  <span class="vh"><?php _e('Close', 'boxpress'); ?></span>
          <svg class="menu-icon-svg" width="20" height="20" focusable="false">
            <use href="#close-icon"/>
          </svg><span class="custom-close"></span></button>
          <?php if ( $paper_author ) :  ?>
            <?php echo $paper_author; ?>
          <?php endif; ?>

          <?php if ( $paper_content ) :  ?>
            <?php echo $paper_content; ?>
          <?php endif; ?>


          <?php if ( $paper_pdf ) :  ?>
           <a class="button pdf--button"
             href="<?php echo esc_url( $paper_pdf['url'] ); ?>"
             target="_blank"> PDF Version
           </a>
         <?php endif; ?>
      </div>
    </div>
  </div>
</div>
