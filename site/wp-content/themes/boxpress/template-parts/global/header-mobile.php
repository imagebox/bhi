<?php
/**
 * Mobile Header
 *
 * @package boxpress
 */
?>
<header class="site-header--mobile" role="banner">
  <div class="mobile-header-left">
    <div class="site-branding">
      <a href="<?php echo esc_url( home_url( '/' )); ?>" rel="home">
        <span class="vh"><?php bloginfo('name'); ?></span>
        <svg class="site-logo" width="261" height="75" focusable="false">
          <use href="#site-logo"/>
        </svg>
      </a>
    </div>
  </div>
  <div class="mobile-header-right">
    <button type="button" class="js-toggle-nav menu-button"
      aria-controls="mobile-nav-tray"
      aria-expanded="false">
      <span class="vh"><?php _e('Menu', 'boxpress'); ?></span>
      <svg class="menu-icon-svg" width="35" height="35" focusable="false">
        <use href="#menu-icon"/>
      </svg>
    </button>
  </div>
</header>

<div id="mobile-nav-tray" class="mobile-nav-tray" aria-hidden="true">
  <div class="mobile-nav-header">
    <div class="tray-logo">
      <a href="<?php echo esc_url( home_url( '/' )); ?>" rel="home">
        <span class="vh"><?php bloginfo('name'); ?></span>
        <svg width="135" height="40" focusable="false">
          <use href="#nav-tray-logo"/>
        </svg>
      </a>
    </div>
    <button type="button" class="js-toggle-nav close-button"
      aria-controls="mobile-nav-tray"
      aria-expanded="false">
      <span class="vh"><?php _e('Close', 'boxpress'); ?></span>
      <svg class="menu-icon-svg" width="35" height="35" focusable="false">
        <use href="#close-icon"/>
      </svg>
    </button>
  </div>
  <nav class="navigation--mobile">

    <?php if ( has_nav_menu( 'primary' )) : ?>
      <ul class="mobile-nav mobile-nav--main">
        <?php
          wp_nav_menu( array(
            'theme_location'  => 'primary',
            'items_wrap'      => '%3$s',
            'container'       => false,
            'walker'          => new Aria_Walker_Nav_Menu(),
          ));
        ?>
      </ul>
    <?php endif; ?>

  <div class="mobile-footer">
    <?php if ( has_nav_menu( 'secondary' )) : ?>
      <ul class="mobile-nav mobile-nav--utility">
        <?php
          wp_nav_menu( array(
            'theme_location'  => 'secondary',
            'items_wrap'      => '%3$s',
            'container'       => false,
            'walker'          => new Aria_Walker_Nav_Menu(),
          ));
        ?>
      </ul>
    <?php endif; ?>
    <div class="footer-social-new">
     <?php get_template_part( 'template-parts/global/social-nav' ); ?>
    </div>
  </div>
  </nav>
</div>
