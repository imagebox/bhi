<?php
/**
 * Displays the Slideshow layout
 *
 * @package boxpress
 */
?>
<?php if ( have_rows( 'home-carousel' )) : ?>

  <div class="home-carousel">


    <video class="bg-video vs-source" playsinline="" autoplay="" muted="" loop="">
        <source src="https://brandywinecommunities.com//wp-content/themes/boxpress/assets/video/hero-vid.mp4" type="video/mp4">
    </video>



    <div class="js-carousel owl-carousel owl-theme">

      <?php while ( have_rows( 'home-carousel' )) : the_row(); ?>
        <?php
          $slide_heading  = get_sub_field( 'slide_heading' );
          $slide_subhead  = get_sub_field( 'slide_subhead' );
          $slide_bkg      = get_sub_field( 'slide_bkg' );
        ?>

        <div class="carousel-slide">

          <?php if ( $slide_bkg ) : ?>
            <?php
              $image_size = 'home_slideshow';
            ?>
            <img class="slide-bkg" draggable="false" aria-hidden="true"
              src="<?php echo $slide_bkg['sizes'][ $image_size ]; ?>"
              width="<?php echo $slide_bkg['sizes'][ $image_size . '-width' ]; ?>"
              height="<?php echo $slide_bkg['sizes'][ $image_size . '-height' ]; ?>"
              alt="">
          <?php endif; ?>

          <div class="wrap slide-wrap">
            <div class="slide-content">

              <h2 class="h1 slide-title"><?php echo $slide_heading; ?></h2>
              <p class="slide-sub-title"><?php echo $slide_subhead; ?></p>

            </div>
          </div>
        </div>

      <?php endwhile; ?>

    </div>
  </div>

<?php endif; ?>
