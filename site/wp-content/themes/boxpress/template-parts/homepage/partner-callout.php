<section class="section partner-section">
  <?php
   $partner_heading = get_field('partner_heading');
   ?>
  <div class="wrap">
    <div class="partner-header">
      <h2><?php echo $partner_heading; ?></h2>
    </div>
    <div class="partner-box">
    <?php if( have_rows('partner_callout_row') ): ?>
    <div class="partner-content">
      <div class="l-grid l-grid--five-col">
      <?php while( have_rows('partner_callout_row') ) : the_row(); ?>
        <?php
          $partner_link = get_sub_field('partner_link');
          $partner_logo = get_sub_field('partner_logo');
        ?>

        <div class="l-grid-item">
          <?php if ( $callout_link ) : ?>
            <a
            href="<?php echo esc_url( $partner_link['url'] ); ?>"
            target="<?php echo esc_attr( $partner_link['target'] ); ?>">
               <img src="<?php echo $partner_logo['url']; ?>" alt="<?php echo $partner_logo['alt']; ?>" />
             </a>
           <?php else : ?>
              <img src="<?php echo $partner_logo['url']; ?>" alt="<?php echo $partner_logo['alt']; ?>" />
           <?php endif; ?>
           </div>

        <?php endwhile; ?>
      </div>
    </div>
  <?php endif; ?>
    </div>
  </div>
</section>
