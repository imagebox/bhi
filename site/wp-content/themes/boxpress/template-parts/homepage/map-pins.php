
<?php



 ?>

<section class="homepage--location-section">
   <div class="row">
     <div class="col-xs-12 col-md-12">
       <div class="box map-box">
         <div class="map-header-desktop">
           <h2>Where we work</h2>
         </div>
        <div class="map-for-desktop">
          <?php echo do_shortcode('[bhimaptwo]');?>
        </div>
        <div class="map-links">
          <ul>
            <li><a href="https://bhi.imagebox.dev/our-work/where-we-work/china/" taget="_blank">China</a></li>
            <li><a href="https://bhi.imagebox.dev/our-work/where-we-work/colombia/" taget="_blank">Columbia</a></li>
            <li><a href="#" taget="_blank">Dominican Republic</a></li>
            <li><a href="https://bhi.imagebox.dev/our-work/where-we-work/el-salvador/" taget="_blank">El Salvador</a></li>
            <li><a href="https://bhi.imagebox.dev/our-work/where-we-work/guatemala-peru-paraguay/" taget="_blank">Guatemala</a></li>
            <li><a href="https://bhi.imagebox.dev/our-work/where-we-work/haiti/" taget="_blank">Haiti</a></li>
            <li><a href="https://bhi.imagebox.dev/our-work/where-we-work/malawi-nigeria/" taget="_blank">Malawi</a></li>
            <li><a href="https://bhi.imagebox.dev/our-work/where-we-work/malawi-nigeria/" taget="_blank">Nigeria</a></li>
            <li><a href="https://bhi.imagebox.dev/our-work/where-we-work/guatemala-peru-paraguay/" taget="_blank">Paraguay</a></li>
            <li><a href="https://bhi.imagebox.dev/our-work/where-we-work/guatemala-peru-paraguay/" taget="_blank">Peru</a></li>
            </li>
          </ul>
        </div>
       </div>
     </div>
   </div>
</section>
