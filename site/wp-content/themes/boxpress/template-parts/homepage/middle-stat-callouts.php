<?php
  $middle_stat_callout_title = get_field('middle_stat_callout_title');
 ?>

<section class="middle-callout-stat">
  <div class="wrap">
    <h2><?php echo $middle_stat_callout_title; ?></h2>
    <?php if( have_rows('middle_stat_callout_row') ): ?>
      <div class="l-grid l-grid--four-col">
      <?php while( have_rows('middle_stat_callout_row') ) : the_row(); ?>

      <?php
        $middle_stat_callout_icon = get_sub_field('middle_stat_callout_icon');
        $middle_stat_callout_stat = get_sub_field('middle_stat_callout_stat');
        $middle_stat_callout_text = get_sub_field('middle_stat_callout_text');
      ?>

      <div class="l-grid-item">
        <img src="<?php echo $middle_stat_callout_icon; ?>" alt="<?php echo $middle_stat_callout_icon; ?>">
        <h3 id="shiva"><span class="odometer" data-animate-number="<?php echo $middle_stat_callout_stat; ?>">0</span></h3>
        <p><?php echo $middle_stat_callout_text; ?></p>
      </div>

        <?php endwhile; ?>
      </div>
    <?php endif; ?>
  </div>
</section>
