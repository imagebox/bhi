<section class="top-shortcut-links">

  <?php
    $home_post_query_args = array(
      'post_type' => 'post',
      'posts_per_page' => 1
    );
    $home_post_query = new WP_Query( $home_post_query_args );
  ?>

  <div class="wrap">
  <?php if ( $home_post_query->have_posts() ) : ?>
    <?php while ( $home_post_query->have_posts() ) : $home_post_query->the_post(); ?>

      <div class="flex-shortcut-container">
        <div class="flex-box">
          <a href="<?php echo get_the_permalink(); ?>">
            <div class="home-blog-thumbnail">
              <?php echo the_post_thumbnail('blog_thumb'); ?>
            </div>
          </a>
        </div>
        <div class="flex-box">
          <a href="<?php echo get_the_permalink(); ?>">
            <div class="flex-box-body">
             <span><?php echo the_date('d.m.y'); ?></span>
              <h3><?php echo the_title(); ?></h3>
              <span class="text-button">Learn More</span>
            </div>
          </a>
        </div>
      </div>
    <?php endwhile; ?>
    <?php wp_reset_postdata(); ?>
  <?php endif; ?>

   <div class="flex-shortcut-container">
     <div class="flex-box-l">
       <?php
       $top_shortcut_link_photo_1 = get_field('top_shortcut_link_photo_1');
       $top_shortcut_link_photo_1_size = "block_half_width";
       $top_shortcut_link_text_1 = get_field('top_shortcut_link_text_1');
       $top_shortcut_link_icon_1 = get_field('top_shortcut_link_icon_1');
       $top_shortcut_link_1 = get_field('top_shortcut_link_1');
        ?>
       <div class="flex-box">
         <a href="<?php echo esc_url( $top_shortcut_link_1['url'] ); ?>"
               target="<?php echo esc_attr( $top_shortcut_link_1['target'] ); ?>">
             <img draggable="false" aria-hidden="true"
               src="<?php echo esc_url( $top_shortcut_link_photo_1['sizes'][ $top_shortcut_link_photo_1_size ] ); ?>"
               width="<?php echo esc_attr( $top_shortcut_link_photo_1['sizes'][ $top_shortcut_link_photo_1_size . '-width' ] ); ?>"
               height="<?php echo esc_attr( $top_shortcut_link_photo_1['sizes'][ $top_shortcut_link_photo_1_size . '-height' ] ); ?>"
               alt="">
               <div class="bottom_shortcut_link_header">
                    <img src="<?php echo $top_shortcut_link_icon_1; ?>" alt="<?php echo $top_shortcut_link_icon_1; ?>">
                   <span><?php echo $top_shortcut_link_1['title']; ?></span>
               </div>
               <div class="bottom_shortcut_link_footer">
                  <p><?php echo $top_shortcut_link_text_1; ?></p>
               </div>
         </a>
       </div>
     </div>

     <div class="flex-box-r">
     <?php if( have_rows('flex_shortcut_row') ): ?>
       <?php while( have_rows('flex_shortcut_row') ) : the_row();

       $top_shortcut_link_photo_2 = get_sub_field('top_shortcut_link_photo_2');
       $top_shortcut_link_photo_2_size = "block_half_width";
       $top_shortcut_link_text_2 = get_sub_field('top_shortcut_link_text_2');
       $top_shortcut_link_icon_2 = get_sub_field('top_shortcut_link_icon_2');
       $top_shortcut_link_2 = get_sub_field('top_shortcut_link_2');

        ?>
        <div class="flex-box">


         <a href="<?php echo esc_url( $top_shortcut_link_2['url'] ); ?>"
               target="<?php echo esc_attr( $top_shortcut_link_2['target'] ); ?>">
             <img draggable="false" aria-hidden="true"
               src="<?php echo esc_url( $top_shortcut_link_photo_2['sizes'][ $top_shortcut_link_photo_2_size ] ); ?>"
               width="<?php echo esc_attr( $top_shortcut_link_photo_2['sizes'][ $top_shortcut_link_photo_2_size . '-width' ] ); ?>"
               height="<?php echo esc_attr( $top_shortcut_link_photo_2['sizes'][ $top_shortcut_link_photo_2_size . '-height' ] ); ?>"
               alt="">
               <div class="bottom_shortcut_link_header">
                    <img src="<?php echo $top_shortcut_link_icon_2; ?>" alt="<?php echo $top_shortcut_link_icon_2; ?>">
                   <span><?php echo $top_shortcut_link_2['title']; ?></span>
               </div>
               <div class="bottom_shortcut_link_footer">
                   <p><?php echo $top_shortcut_link_text_2; ?></p>
               </div>
         </a>
         </div>
     <?php endwhile; ?>
   <?php endif; ?>
   </div>
   </div>
  </div>
</section>
