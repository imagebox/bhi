<?php
/**
 * Displays the Hero layout
 *
 * Easily convertible into a sideshow by enabling multiple
 * rows in the repeater.
 *
 * @package boxpress
 */
?>
<?php if ( have_rows( 'homepage_hero' )) : ?>
  <?php while ( have_rows( 'homepage_hero' )) : the_row();
      $hero_content     = get_sub_field( 'hero_content' );
      $hero_background  = get_sub_field( 'hero_background' );
      $hero_background_image = get_sub_field( 'hero_background_image' );
    ?>

    <section class="hero <?php echo $hero_background; ?>">
      <div class="overlay"></div>
      <div class="wrap">
        <div class="hero-box">
          <div class="hero-content">
           <?php echo $hero_content; ?>
          </div>
        </div>
      </div>



      <video class="bg-video vs-source" playsinline="" autoplay="" muted="" loop="">
          <source src="https://bhi.imagebox.dev/wp-content/themes/boxpress/assets/video/bhi-hero-vid-2.mp4" type="video/mp4">
      </video>

      <?php if ( $hero_background_image && $hero_background === 'background-image' ) : ?>
        <img class="hero-bkg-image" draggable="false" aria-hidden="true"
          src="<?php echo esc_url( $hero_background_image['url'] ); ?>"
          width="<?php echo esc_attr( $hero_background_image['width'] ); ?>"
          height="<?php echo esc_attr( $hero_background_image['height'] ); ?>"
          alt="<?php echo esc_attr( $hero_background_image['alt'] ); ?>">
      <?php endif; ?>
    </section>

  <?php endwhile; ?>
<?php endif; ?>
