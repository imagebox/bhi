<?php
$bottom_callout_photo = get_field('bottom_callout_photo');
$bottom_callout_photo_size = "hero_full_width";
$bottom_callout_text = get_field('bottom_callout_text');
 ?>

<section class="bottom-callout">
  <div class="wrap">
  <div class="orb-container">
    <div class="callout-photo">
      <img draggable="false" aria-hidden="true"
        src="<?php echo esc_url( $bottom_callout_photo['sizes'][ $bottom_callout_photo_size ] ); ?>"
        width="<?php echo esc_attr( $bottom_callout_photo['sizes'][ $bottom_callout_photo_size . '-width' ] ); ?>"
        height="<?php echo esc_attr( $bottom_callout_photo['sizes'][ $bottom_callout_photo_size . '-height' ] ); ?>"
        alt="">
    </div>
  </div>
    <div class="callout-text">
      <?php echo $bottom_callout_text; ?>
    </div>
  </div>
</section>
