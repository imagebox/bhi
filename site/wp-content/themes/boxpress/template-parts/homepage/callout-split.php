<section class="home-callout-split">
  <?php
    $bottom_shortcut_link_photo = get_field('bottom_shortcut_link_photo');
    $bottom_shortcut_link_photo_size = "block_full_width_lg";
    $bottom_shortcut_link_icon = get_field('bottom_shortcut_link_icon');
    $home_split_copy = get_field('home_split_copy');
  ?>
  <div class="box-one">
    <img draggable="false" aria-hidden="true"
      src="<?php echo esc_url( $bottom_shortcut_link_photo['sizes'][ $bottom_shortcut_link_photo_size ] ); ?>"
      width="<?php echo esc_attr( $bottom_shortcut_link_photo['sizes'][ $bottom_shortcut_link_photo_size . '-width' ] ); ?>"
      height="<?php echo esc_attr( $bottom_shortcut_link_photo['sizes'][ $bottom_shortcut_link_photo_size . '-height' ] ); ?>"
      alt="">
  </div>
  <div class="box-two">
    <div class="split-wrap">
    <div class="bottom_shortcut_link_header">
         <img src="<?php echo $bottom_shortcut_link_icon; ?>" alt="<?php echo $bottom_shortcut_link_icon; ?>">
        <span>Stories</span>
    </div>
    <div class="shortcut_copy">
        <?php echo $home_split_copy; ?>
      </div>
    </div>
  </div>
</section>
