<section class="top-shortcut-links">

  <?php
    $home_post_query_args = array(
      'post_type' => 'post',
      'posts_per_page' => 1
    );
    $home_post_query = new WP_Query( $home_post_query_args );
  ?>

  <div class="wrap">
  <?php if ( $home_post_query->have_posts() ) : ?>
    <?php while ( $home_post_query->have_posts() ) : $home_post_query->the_post(); ?>

      <div class="flex-shortcut-container">
        <div class="flex-box">
          <a href="<?php echo get_the_permalink(); ?>">
            <div class="home-blog-thumbnail">
              <?php echo the_post_thumbnail('blog_thumb'); ?>
            </div>
          </a>
        </div>
        <div class="flex-box">
          <a href="<?php echo get_the_permalink(); ?>">
            <div class="flex-box-body">
             <span><?php echo the_date('d.m.y'); ?></span>
              <h3><?php echo the_title(); ?></h3>
              <span class="text-button">Learn More</span>
            </div>
          </a>
        </div>
      </div>
    <?php endwhile; ?>
    <?php wp_reset_postdata(); ?>
  <?php endif; ?>
   </div>
   </div>
  </div>
</section>
