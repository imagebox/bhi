<section class="section home-cpt">
  <div class="wrap">
    <div class="l-grid l-grid--three-col">
      <div class="l-grid-item">
        <div class="box">
          <?php
          $home_press_query_args = array(
            'post_type' => 'press',
            'posts_per_page' => 3
          );
          $home_press_query_args = new WP_Query( $home_press_query_args );

          ?>

          <h1>Press</h1>
          <?php if ( $home_press_query_args->have_posts() ) : ?>
              <?php while ( $home_press_query_args->have_posts() ) : $home_press_query_args->the_post();

              $press_link = get_field('press_link');
              $press_watch_link = get_field('press_watch_link');
              $press_text = get_field('press_text');

              ?>
                 <div class="home-post-box">
                   <div class="post-thumbnail">
                     <?php the_post_thumbnail('blog-thumb');?>
                   </div>
                   <div class="post-body">
                     <span><?php echo $press_text; ?></span>
                     <h3><?php echo wp_trim_words( strip_shortcodes (get_the_title()), 8, '&hellip;' ); ?></h3>
                    <div class="button-box">
                      <?php if ( $press_link ) : ?>
                      <a class="button button--green"
                        href="<?php echo esc_url( $press_link['url'] ); ?>"
                        target="<?php echo esc_attr( $press_link['target'] ); ?>">
                        Learn More
                      </a>
                      <?php endif; ?>
                      <?php if ( $press_watch_link ) : ?>
                      <a class="button button--purple"
                        href="<?php echo esc_url( $press_watch_link['url'] ); ?>"
                        target="<?php echo esc_attr( $press_watch_link['target'] ); ?>">
                        Watch it here
                      </a>
                      <?php endif; ?>
                    </div>
                   </div>
                 </div>
              <?php endwhile; ?>
            <?php wp_reset_postdata(); ?>
           <?php endif; ?>

        </div>
      </div>
      <div class="l-grid-item">
        <div class="box">
          <h1>Highlights</h1>
          <?php
            $home_post_2_query_args = array(
              'post_type' => 'post',
              'posts_per_page' => 3
            );
            $home_post_2_query_args = new WP_Query( $home_post_2_query_args );
          ?>

          <?php if ( $home_post_2_query_args->have_posts() ) : ?>
              <?php while ( $home_post_2_query_args->have_posts() ) : $home_post_2_query_args->the_post();

              ?>
                 <div class="home-post-box">
                   <div class="post-thumbnail">
                     <?php the_post_thumbnail();?>
                   </div>
                   <div class="post-body">
                     <span><?php the_date('F, Y'); ?></span>
                     <h3><?php echo wp_trim_words( strip_shortcodes (get_the_title()), 8, '&hellip;' ); ?></h3>
                    <div class="button-box">
                      <a class="button button--purple" href="<?php echo get_the_permalink(); ?>">Learn More</a>
                    </div>
                   </div>
                 </div>
              <?php endwhile; ?>
            <?php wp_reset_postdata(); ?>
           <?php endif; ?>


        </div>
      </div>
      <div class="l-grid-item">
        <div class="box">
         <?php
           $home_event_photo_size = "block_full_width_lg";
           $home_event_photo = get_field('home_event_photo');
           $home_event_copy = get_field('home_event_copy');
          ?>
        <div class="home-box-event">
          <img draggable="false" aria-hidden="true"
            src="<?php echo esc_url( $home_event_photo['sizes'][ $home_event_photo_size ] ); ?>"
            width="<?php echo esc_attr( $home_event_photo['sizes'][ $home_event_photo_size . '-width' ] ); ?>"
            height="<?php echo esc_attr( $home_event_photo['sizes'][ $home_event_photo_size . '-height' ] ); ?>"
            alt="">
          <div class="home-box-copy">
              <?php echo $home_event_copy; ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
