<?php
/**
 * Displays the page banner
 *
 * @package boxpress
 */

$banner_title     = get_the_title();
$banner_image_url = '';
$default_banner   = get_field( 'default_banner_image', 'option' );
$bhi_board_banner   = get_field( 'bhi_board_banner', 'option' );

  if ( $guideline_banner ) {
    $banner_image_url = $bhi_board_banner['url'];
  } elseif ( $default_banner ) {
    $banner_image_url = $default_banner['url'];
  }

?>
<header class="banner">
  <div class="wrap">
    <div class="banner-title">
      <span class="h1">
          About
      </span>
    </div>
    <?php if ( ! empty( $banner_image_url )) : ?>
      <img class="banner-image" draggable="false" src="<?php echo $banner_image_url; ?>" alt="">
    <?php endif; ?>
  </div>
</header>
