<?php
/**
 * Displays the page banner
 *
 * @package boxpress
 */

$banner_title     = get_the_title();
$banner_image_url = '';
$default_banner   = get_field( 'default_banner_image', 'option' );
$press_banner   = get_field( 'press_banner', 'option' );

  if ( $press_banner ) {
    $banner_image_url = $press_banner['url'];
  } elseif ( $default_banner ) {
    $banner_image_url = $default_banner['url'];
  }

?>
<header class="banner">
  <div class="wrap">
    <div class="banner-title">
      <span class="h1">
          Resources
      </span>
    </div>
    <?php if ( ! empty( $banner_image_url )) : ?>
      <img class="banner-image" draggable="false" src="<?php echo $banner_image_url; ?>" alt="">
    <?php endif; ?>
  </div>
</header>
