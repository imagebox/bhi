<?php
/**
 * WordPress Editor related functions
 *
 * @package boxpress
 */

/**
 * Editor Formats
 */

function boxpress_mce_buttons( $buttons ) {
  array_unshift( $buttons, 'styleselect' );
  return $buttons;
}
add_filter( 'mce_buttons_2', 'boxpress_mce_buttons' );

function boxpress_mce_before_init_insert_formats( $init_array ) {
  $style_formats = array(
    array(
      'title'     => 'Lead Paragraph',
      'selector'  => 'p',
      'classes'   => 'p-lead',
    ),
    array(
      'title'     => 'Teal Button',
      'selector'  => 'a',
      'classes'   => 'button button--green',
    ),
    array(
      'title'     => 'Purple Button',
      'selector'  => 'a',
      'classes'   => 'button button--purple',
    ),
    array(
      'title'     => 'White Button',
      'selector'  => 'a',
      'classes'   => 'button button--white',
    ),
    array(
      'title'     => 'Download Button',
      'selector'  => 'a',
      'classes'   => 'button pdf--button',
    ),
    array(
      'title'     => 'Text Button',
      'selector'  => 'a',
      'classes'   => 'text-button',
    ),
  );
  $init_array['style_formats'] = json_encode( $style_formats );

  return $init_array;
}
add_filter( 'tiny_mce_before_init', 'boxpress_mce_before_init_insert_formats' );



/**
 * Editor Styles
 */

function boxpress_theme_add_editor_styles() {
  add_editor_style( 'custom-editor-style.css' );
}
add_action( 'admin_init', 'boxpress_theme_add_editor_styles' );
