<?php
/**
 * Advanced Custom Fields Options Page Settings
 *
 * @link http://www.advancedcustomfields.com/resources/options-page/
 *
 * @package boxpress
 */
if( function_exists('acf_add_options_page') ) {

  /**
   * Main Options Page
   */

  acf_add_options_page(array(
    'page_title'  => 'Options',
    'menu_title'  => 'Options',
    'menu_slug'   => 'options-page',
    'capability'  => 'edit_posts',
  ));


  /**
   * Sub-Pages
   */

  acf_add_options_sub_page(array(
    'page_title'  => 'Social Media',
    'menu_title'  => 'Social Media',
    'parent_slug' => 'options-page',
  ));

  acf_add_options_sub_page(array(
    'page_title'  => 'Google Analytics',
    'menu_title'  => 'Google Analytics',
    'parent_slug' => 'options-page',
  ));

  acf_add_options_sub_page(array(
    'page_title'  => 'Business Address',
    'menu_title'  => 'Business Address',
    'parent_slug' => 'options-page',
  ));

  acf_add_options_sub_page(array(
    'page_title'  => 'Page Options',
    'menu_title'  => 'Page Options',
    'parent_slug' => 'options-page',
  ));
}
