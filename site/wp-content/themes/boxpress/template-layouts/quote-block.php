<?php
/**
 * Displays the quote block layout
 *
 * @package boxpress
 */

$quote_copy       = get_sub_field( 'quote_copy' );
$quote_citation   = get_sub_field( 'quote_citation' );
$quote_job_title  = get_sub_field( 'quote_job_title' );
$quote_bkg        = get_sub_field( 'quote_background' );
$quote_bkg_image  = get_sub_field( 'quote_background_image' );
$quote_bkg_size   = 'block_full_width';

?>
<section class="quote-block-layout section <?php echo $quote_bkg; ?>">
  <div class="wrap wrap--limited">

    <blockquote class="quote-block">
      <div class="quote-block-body">
        <?php echo $quote_copy; ?>
      </div>

      <?php if ( ! empty( $quote_citation ) ) : ?>

        <cite class="quote-block-citation">
          <span class="quote-name"><?php echo $quote_citation; ?></span>
          <?php if ( ! empty( $quote_job_title )) : ?>
            <span class="quote-title"><?php echo $quote_job_title; ?></span>
          <?php endif; ?>
        </cite>

      <?php endif; ?>

    </blockquote>

  </div>

  <?php if ( $quote_bkg_image && $quote_bkg === 'background-image' ) : ?>
    <img class="quote-block-layout-bkg" draggable="false" aria-hidden="true"
      src="<?php echo esc_url( $quote_bkg_image['sizes'][ $quote_bkg_size ] ); ?>"
      width="<?php echo esc_attr( $quote_bkg_image['sizes'][ $quote_bkg_size . '-width' ] ); ?>"
      height="<?php echo esc_attr( $quote_bkg_image['sizes'][ $quote_bkg_size . '-height' ] ); ?>"
      alt="">
  <?php endif; ?>

</section>
