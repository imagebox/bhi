<?php
/**
 * Displays the slipt content / image layout
 *
 * @package boxpress
 */

$onethird_twothird_content    = get_sub_field( 'onethird_twothird_content' );
$onethird_twothird_image      = get_sub_field( 'onethird_twothird_image' );
$onethird_twothird_image_position = get_sub_field( 'onethird_twothird_image_position' );
$onethird_twothird_image_size = 'block_half_width';

?>

<section class="section onethird">
  <div class="wrap">
    <div class="split-block-layout <?php echo $onethird_twothird_bkg; ?> split-block-layout--<?php echo $onethird_twothird_image_position; ?>">
      <div class="split-block-col photo-content">

        <?php if ( $onethird_twothird_image ) : ?>

          <img
            src="<?php echo esc_url( $onethird_twothird_image['sizes'][ $onethird_twothird_image_size ] ); ?>"
            width="<?php echo esc_attr( $onethird_twothird_image['sizes'][ $onethird_twothird_image_size . '-width'] ); ?>"
            height="<?php echo esc_attr( $onethird_twothird_image['sizes'][ $onethird_twothird_image_size . '-height'] ); ?>"
            alt="<?php echo esc_attr( $onethird_twothird_image['alt'] ); ?>">

        <?php endif; ?>

      </div>
      <div class="split-block-col text-content">
        <div class="split-block-content">
          <div class="split-block-content-inner-wrap">
            <?php echo $onethird_twothird_content; ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
