<?php
/**
 * Displays the full width column layout
 *
 * @package boxpress
 */

$content_block      = get_sub_field( 'content_block' );
$content_bkg        = get_sub_field( 'content_block_background' );
$content_bkg_image  = get_sub_field( 'content_block_background_image' );
$content_bkg_size   = 'block_full_width';

// Load sidebar if this is the first template & child pages exist
$is_first_row = ( $row_index == 1 ) ? true : false;

if ( $is_first_row ) {
  $child_pages_list = query_for_child_page_list();
} else {
  // Empty the child list array to prevent false positive
  $child_pages_list = array();
}

?>
<?php if ( ! empty( $content_block )) : ?>

  <section class="fullwidth-layout advanced-full-width section <?php echo $content_bkg; ?>">
    <div class="wrap <?php
        if ( ! $child_pages_list ) {
          echo ' wrap--limited ';
        }
      ?>">
      <div class="<?php
          if ( $child_pages_list ) {
            echo 'l-sidebar';
          }
        ?>">
        <div class="l-main-col">
          <?php if ( $is_first_row ) : ?>
            <?php
              $media_headline = get_field( 'media_headline', get_the_ID() );
              $page_title = ( ! empty( $media_headline )) ? $media_headline : get_the_title( get_the_ID() );
            ?>
            <header class="page-header">
              <h1 class="page-title"><?php echo $page_title; ?></h1>
            </header>
          <?php endif; ?>
          <div class="page-content">
            <?php echo $content_block; ?>
          </div>
        </div>

        <?php if ( $child_pages_list ) : ?>
          <div class="l-aside-col">
            <?php get_sidebar(); ?>
          </div>
        <?php endif; ?>

      </div>
    </div>

    <?php if ( $content_bkg_image && $content_bkg === 'background-image' ) : ?>
      <img class="fullwidth-layout-bkg" draggable="false" aria-hidden="true"
        src="<?php echo esc_url( $content_bkg_image['url'] ); ?>"
        width="<?php echo esc_attr( $content_bkg_image['width'] ); ?>"
        height="<?php echo esc_attr( $content_bkg_image['height'] ); ?>"
        alt="">
    <?php endif; ?>

  </section>

<?php endif; ?>
