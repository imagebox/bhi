<?php
/**
 * Displays the Two Third Col layout
 */

$section_heading = get_sub_field( 'section_heading' );
$one_third_position = get_sub_field( 'one_third_column_position' );
$two_thirds_content = get_sub_field( 'two_thirds_content' );
$one_third_content  = get_sub_field( 'one_third_content' );
$background = get_sub_field( 'background' );

$remove_top_padding   = get_sub_field( 'remove_top_padding' );
$remove_divider_line  = get_sub_field( 'remove_divider_line' );

$heading_link = get_sub_field( 'heading_link' );

?>


<section class="fullwidth-column section
  <?php
    if ( $remove_divider_line ) {
      echo 'section--no-divider ';
    }
    if ( $remove_top_padding ) {
      echo 'section--no-top-pad ';
    }
  ?>
  <?php echo $background; ?>">

  <div id="section-<?php echo $row_index; ?>" class="article-anchor"></div>
  <div class="wrap wrap--limited">

    <?php if ( ! empty( $section_heading )) : ?>
      <header class="section-header slide-up-fade-in">
        <h2 class="large-h2">
          <?php if ( $heading_link ) : ?>
            <a href="<?php echo esc_url( $heading_link['url'] ); ?>" target="<?php echo $heading_link['target']; ?>">
          <?php endif; ?>

          <?php echo $section_heading; ?>

          <?php if ( $heading_link ) : ?>
            </a>
          <?php endif; ?>
        </h2>
      </header>
    <?php endif; ?>

    <div class="columns-two-thirds columns-two-thirds--<?php echo $one_third_position; ?>">
      <div class="col-two-third">
        <div class="page-content slide-up-fade-in">
          <?php echo $two_thirds_content; ?>
        </div>
      </div>
      <div class="col-one-third">
        <div class="page-content slide-up-fade-in">
          <?php echo $one_third_content; ?>
        </div>
      </div>
    </div>

  </div>
</section>
