<?php
/**
 * BoxPress functions and definitions
 *
 * @package boxpress
 */

if ( ! function_exists( 'boxpress_setup' )) :
function boxpress_setup() {

  add_theme_support( 'automatic-feed-links' );
  add_theme_support( 'title-tag' );
  add_theme_support( 'post-thumbnails', array( 'post', 'page', 'press', 'team', 'events', 'events' ));
  add_theme_support( 'html5', array(
    'search-form',
    'comment-form',
    'comment-list',
    'gallery',
    'caption',
  ));

  add_image_size( 'home_slideshow', 1200, 600, true );
  add_image_size( 'home_index_thumb', 860, 350, true );
  add_image_size( 'blog_thumb', 500, 400, true );
  add_image_size( 'ann_report', 1500, 1500, true );

  add_image_size( 'block_full_width', 1600, 500, true );
  add_image_size( 'block_full_width_lg', 1600, 1000, true );
  add_image_size( 'block_half_width', 800, 500, true );
  add_image_size( 'hero_full_width', 900, 900, true );
}
endif;
add_action( 'after_setup_theme', 'boxpress_setup' );



/**
 * Enqueue scripts and styles.
 */

function boxpress_scripts() {

  /**
   * Styles
   */



  // Google Fonts - Replace with your fonts URL
  $style_font_url = 'https://fonts.googleapis.com/css2?family=Barlow:wght@400;600';
  wp_enqueue_style( 'google-fonts', $style_font_url, array(), false, 'screen' );

  // Google Fonts - Replace with your fonts URL
  $style_font_url_2 = 'https://fonts.googleapis.com/css2?family=Dosis:wght@400;700;800;';
  wp_enqueue_style( 'google-fonts', $style_font_url_2, array(), false, 'screen' );

  // Main screen styles
  $style_screen_path  = get_template_directory_uri() . '/assets/css/style.min.css';
  $style_screen_ver   = filemtime( get_template_directory() . '/assets/css/style.min.css' );
  wp_enqueue_style( 'screen', $style_screen_path, array('google-fonts'), $style_screen_ver, 'screen' );


  // Main print styles
  $style_print_path   = get_template_directory_uri() . '/assets/css/print.min.css';
  $style_print_ver    = filemtime( get_template_directory() . '/assets/css/print.min.css' );
  wp_enqueue_style( 'print', $style_print_path, array( 'screen' ), $style_print_ver, 'print' );




  /**
   * Scripts
   */

  // HTML5 Shiv - Only load for <IE9
  $script_html5shiv_path  = get_template_directory_uri() . '/assets/js/dev/html5shiv-printshiv.min.js';
  $script_html5shiv_ver   = filemtime( get_template_directory() . '/assets/js/dev/html5shiv-printshiv.min.js' );
  wp_enqueue_script( 'html5shiv', $script_html5shiv_path, array(), $script_html5shiv_ver, false );
  wp_script_add_data( 'html5shiv', 'conditional', 'lt IE 9' );

  // Main site scripts
  $script_site_path = get_template_directory_uri() . '/assets/js/build/site.min.js';
  $script_site_ver  = filemtime( get_template_directory() . '/assets/js/build/site.min.js' );
  wp_enqueue_script( 'site', $script_site_path, array( 'jquery' ), $script_site_ver, true );



  // Single Posts
  if ( is_singular() && comments_open() &&
       get_option( 'thread_comments' )) {
    wp_enqueue_script( 'comment-reply' );
  }
}
add_action( 'wp_enqueue_scripts', 'boxpress_scripts' );



/**
 * Query for child pages of current page
 *
 * @param  array  $options Accepts post type, depth and page ID.
 * @return string          Returns WP list of pages in html.
 */
function query_for_child_page_list( $options = array() ) {
  $default_options = array(
    'post_type' => 'page',
    'depth'     => 4,
    'page_id'   => '',
  );
  $config = array_merge( $default_options, $options );

  global $wp_query;
  $post = $wp_query->post;

  if ( $post ) {
    $parent_ID = $post->ID;

    if ( ! empty( $config['page_id'] )) {
      $parent_ID = $config['page_id'];
    }

    if ( $post->post_parent !== 0 ) {
      $ancestors  = get_post_ancestors( $post );
      $parent_ID  = end( $ancestors );
    }

    $list_pages_args = array(
      'post_type' => $config['post_type'],
      'title_li'  => '',
      'child_of'  => $parent_ID,
      'depth'     => $config['depth'],
      'echo'      => false,
    );

    // Get list of pages
    return wp_list_pages( $list_pages_args );
  }
}


/**
 * Numbered Pagination
 */
function boxpress_pagination( $current_query = array() ) {
  $format = '?paged=%#%';

  // Settings for using a main query
  if ( ! $current_query ) {
    global $wp_query;
    $current_query = $wp_query;
    $format = 'page/%#%/';
  }

  $max_num_pages  = $current_query->max_num_pages;
  $found_posts    = $current_query->found_posts;
  $posts_per_page = $current_query->posts_per_page;
  $big        = 999999999;
  $prev_arrow = is_rtl() ? '→' : '←';
  $next_arrow = is_rtl() ? '←' : '→';

  if ( $max_num_pages > 1 && $found_posts > $posts_per_page )  {
    echo '<nav class="pagination" role="navigation" aria-label="Pagination Navigation">';

    echo paginate_links(array(
      'base'      => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
      'format'    => $format,
      'current'   => max( 1, get_query_var( 'paged' )),
      'aria_current' => 'true',
      'total'     => $max_num_pages,
      'mid_size'  => 3,
      'type'      => 'list',
      'prev_text' => $prev_arrow,
      'next_text' => $next_arrow,
    ));

    echo '</nav>';
  }
}


/**
 * Menu setup
 */
require get_template_directory() . '/inc/site-navigation.php';

/**
 * Accessibility
 */
require get_template_directory() . '/inc/walkers/class-aria-walker-nav-menu.php';

/**
 * Image Gallery - *Optional*
 */
require get_template_directory() . '/inc/image-gallery.php';

/**
 * Cleanup the header
 */
require get_template_directory() . '/inc/cleanup.php';

/**
 * CPT
 */
 require get_template_directory() . '/inc/cpt/cpt-press.php';
 require get_template_directory() . '/inc/cpt/cpt-publication.php';
 require get_template_directory() . '/inc/cpt/cpt-guideline.php';
 require get_template_directory() . '/inc/cpt/cpt-board-members.php';
 require get_template_directory() . '/inc/cpt/cpt-team.php';
 require get_template_directory() . '/inc/cpt/cpt-events.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Admin Related Functions
 */
require get_template_directory() . '/inc/admin.php';
require get_template_directory() . '/inc/editor.php';







/**
 * ACF
 */
// Options Pages
require get_template_directory() . '/inc/acf-options.php';
// Search Queries
require get_template_directory() . '/inc/acf-search.php';

/**
 * Plugin Settings
 */
require get_template_directory() . '/inc/plugin-settings/gravity-forms-settings.php';
require get_template_directory() . '/inc/plugin-settings/tribe-events-settings.php';





function revcon_change_post_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = 'Highlights';
    $submenu['edit.php'][5][0] = 'Highlights';
    $submenu['edit.php'][10][0] = 'Add Highlights';
    $submenu['edit.php'][16][0] = 'Highlights Tags';
}
function revcon_change_post_object() {
    global $wp_post_types;
    $labels = &$wp_post_types['post']->labels;
    $labels->name = 'Highlights';
    $labels->singular_name = 'Highlights';
    $labels->add_new = 'Add Highlights';
    $labels->add_new_item = 'Add Highlights';
    $labels->edit_item = 'Edit Highlights';
    $labels->new_item = 'Highlights';
    $labels->view_item = 'View Highlights';
    $labels->search_items = 'Search Highlights';
    $labels->not_found = 'No Highlights found';
    $labels->not_found_in_trash = 'No Highlights found in Trash';
    $labels->all_items = 'All Highlights';
    $labels->menu_name = 'Highlights';
    $labels->name_admin_bar = 'Highlights';
}

add_action( 'admin_menu', 'revcon_change_post_label' );
add_action( 'init', 'revcon_change_post_object' );




function diwp_create_shortcode_donor_post_type(){

    $args = array(
                    'post_type'      => 'post',
                    'posts_per_page' => '3',
                    'publish_status' => 'published',
                    'category_name' => 'donors',
                    'order_by' => 'rand',
                 );


    $query = new WP_Query($args);


    if($query->have_posts()) :

        while($query->have_posts()) :

            $query->the_post() ;

            $result .= '<ul>';
            $result .= '<li>';
            $result .= '<a href="' . get_permalink() . '">';
            $result .= get_the_title() ;
            $result .= '</a>';
            $result .= '</li>';
            $result .= '</ul>';

        endwhile;

        wp_reset_postdata();

    endif;

    return $result;
}

add_shortcode( 'donors-list', 'diwp_create_shortcode_donor_post_type' );


// Donors

function diwp_create_shortcode_academic_post_type(){

    $args = array(
                    'post_type'      => 'post',
                    'posts_per_page' => '3',
                    'publish_status' => 'published',
                    'category_name' => 'academic-institutions	',
                    'order_by' => 'rand',
                 );


    $query = new WP_Query($args);


    if($query->have_posts()) :

        while($query->have_posts()) :

            $query->the_post() ;

            $result .= '<ul>';
            $result .= '<li>';
            $result .= '<a href="' . get_permalink() . '">';
            $result .= get_the_title() ;
            $result .= '</a>';
            $result .= '</li>';
            $result .= '</ul>';

        endwhile;

        wp_reset_postdata();

    endif;

    return $result;
}

add_shortcode( 'academic-institution-list', 'diwp_create_shortcode_academic_post_type' );

// Academic


function diwp_create_shortcode_non_gov_org_list_post_type(){

    $args = array(
                    'post_type'      => 'post',
                    'posts_per_page' => '3',
                    'publish_status' => 'published',
                    'category_name' => 'non-government-organizations',
                    'order_by' => 'rand',
                 );


    $query = new WP_Query($args);


    if($query->have_posts()) :

        while($query->have_posts()) :

            $query->the_post() ;

            $result .= '<ul>';
            $result .= '<li>';
            $result .= '<a href="' . get_permalink() . '">';
            $result .= get_the_title() ;
            $result .= '</a>';
            $result .= '</li>';
            $result .= '</ul>';

        endwhile;

        wp_reset_postdata();

    endif;

    return $result;
}
add_shortcode( 'non-gov-org-list', 'diwp_create_shortcode_pharma_post_type' );

// Non-Government Organizations


function diwp_create_shortcode_pharma_post_type(){

    $args = array(
                    'post_type'      => 'post',
                    'posts_per_page' => '3',
                    'publish_status' => 'published',
                    'category_name' => 'pharma-medical-technology-companies',
                    'order_by' => 'rand',
                 );


    $query = new WP_Query($args);


    if($query->have_posts()) :

        while($query->have_posts()) :

            $query->the_post() ;

            $result .= '<ul>';
            $result .= '<li>';
            $result .= '<a href="' . get_permalink() . '">';
            $result .= get_the_title() ;
            $result .= '</a>';
            $result .= '</li>';
            $result .= '</ul>';

        endwhile;

        wp_reset_postdata();

    endif;

    return $result;
}
add_shortcode( 'pharma-medical-technology-companies-list', 'diwp_create_shortcode_pharma_post_type' );

// pharma-medical-technology-companies-list


function diwp_create_shortcode_gov_inst_post_type(){

    $args = array(
                    'post_type'      => 'post',
                    'posts_per_page' => '3',
                    'publish_status' => 'published',
                    'category_name' => 'government-institutions	',
                    'order_by' => 'rand',
                 );


    $query = new WP_Query($args);


    if($query->have_posts()) :

        while($query->have_posts()) :

            $query->the_post() ;

            $result .= '<ul>';
            $result .= '<li>';
            $result .= '<a href="' . get_permalink() . '">';
            $result .= get_the_title() ;
            $result .= '</a>';
            $result .= '</li>';
            $result .= '</ul>';

        endwhile;

        wp_reset_postdata();

    endif;

    return $result;
}
add_shortcode( 'gov-institution-list', 'diwp_create_shortcode_gov_inst_post_type' );

// gov-institution-list



function diwp_create_shortcode_patients_fam_post_type(){

    $args = array(
                    'post_type'      => 'post',
                    'posts_per_page' => '3',
                    'publish_status' => 'published',
                    'category_name' => 'patients-and-families',
                    'order_by' => 'rand',
                 );


    $query = new WP_Query($args);


    if($query->have_posts()) :

        while($query->have_posts()) :

            $query->the_post() ;

            $result .= '<ul>';
            $result .= '<li>';
            $result .= '<a href="' . get_permalink() . '">';
            $result .= get_the_title() ;
            $result .= '</a>';
            $result .= '</li>';
            $result .= '</ul>';

        endwhile;

        wp_reset_postdata();

    endif;

    return $result;
}
add_shortcode( 'patient-fam-list', 'diwp_create_shortcode_patients_fam_post_type' );

// Pharma & Medical Technology Companies


/**
 * Filter the except length to 20 words.
 *
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
 */
function wpdocs_custom_excerpt_length( $length ) {
    return 20;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );
