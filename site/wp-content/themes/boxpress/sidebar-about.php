<?php
/**
 * Displays the sidebar
 *
 * @package boxpress
 */
?>
<aside class="sidebar" role="complementary">


      <div class="sidebar-widget">
        <nav class="sidebar-nav">
          <ul>

            <?php
            wp_list_pages( array(
                'title_li'    => '',
                'child_of'    => 193,
                'show_date'   => 'modified',
                'date_format' => $date_format
            ) );
            ?>

          </ul>
        </nav>
      </div>



</aside>
