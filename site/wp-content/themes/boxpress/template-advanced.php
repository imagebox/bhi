<?php
/**
 * Template Name: Advanced
 *
 * Page template to display the advanced page builder.
 *
 * @package boxpress
 */
get_header(); ?>

<?php require_once('template-parts/banners/banner--page.php'); ?>

  <?php
    /**
     * Run clean shortcode for repeater sub-fields.
     *
     * Needs to be calld directly before repeaters, won't
     * work directly in functions.php because of how
     * sub-fields are called.
     */
    function boxpress_clean_shortcodes_acf( $content ) {

      // els to remove
      $array = array(
        '<p>['    => '[',
        ']</p>'   => ']',
        '<div>['  => '[',
        ']</div>' => ']',
        ']<br />' => ']',
        ']<br>'   => ']',
        '<br />[' => '[',
        '<br>['   => '[',
      );

      // Remove dem els
      $content = strtr( $content, $array );
      return $content;
    }
    add_filter('acf_the_content', 'boxpress_clean_shortcodes_acf');
  ?>

  <?php if ( have_rows( 'innerpage_master' )) :
      $row_index = 1;
    ?>

    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

      <?php while ( have_rows( 'innerpage_master' )) : the_row(); ?>

        <?php
          // Create layout string
          $layout_path = 'template-layouts/' . str_replace( '_', '-', get_row_layout()) . '.php';

          // Include our layout
          if (( @include $layout_path ) === false ) {
            // If the layout block is missing, display an error block only for admins
            if ( current_user_can('edit_others_pages') ) {
              echo '<div class="section boxpress-error"><div class="wrap wrap--limited"><p>Sorry bud, that layout file is missing!</p></div></div>';
            }
          }

          $row_index++;
        ?>

      <?php endwhile; ?>
      <?php wp_reset_postdata(); ?>

      <?php
        // Display sidebar at bottom of page for mobile
        $child_pages_list = query_for_child_page_list();
      ?>
      <?php if ( $child_pages_list ) : ?>

        <div class="advanced-sidebar">
          <div class="wrap">
            <?php include( get_template_directory() . '/sidebar.php'); ?>
          </div>
        </div>

      <?php endif; ?>

    </article>

  <?php endif; ?>

<?php get_footer(); ?>
