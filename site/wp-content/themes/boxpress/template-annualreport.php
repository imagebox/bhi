<?php
/**
 * Template Name: Annual Reports
 *
 * Page template to display the advanced page builder.
 *
 * @package BoxPress
 */

 // Load sidebar if this is the first template & child pages exist
 $is_first_row = ( $row_index == 1 ) ? true : false;

 if ( $is_first_row ) {
   $child_pages_list = query_for_child_page_list();
 } else {
   // Empty the child list array to prevent false positive
   $child_pages_list = array();
 }


get_header(); ?>

<?php require_once('template-parts/banners/banner--page.php'); ?>

  <section class="section annual-report-section">
      <div class="wrap">
        <div class="l-sidebar">
          <div class="l-main-col">
            <?php if ( $is_first_row ) : ?>
              <?php
                $media_headline = get_field( 'media_headline', get_the_ID() );
                $page_title = ( ! empty( $media_headline )) ? $media_headline : get_the_title( get_the_ID() );
              ?>
              <header class="page-header">
                <h1 class="page-title"><?php echo $page_title; ?></h1>
              </header>
            <?php endif; ?>


            <?php if( have_rows('annual_reports') ): ?>

              <div class="l-grid l-grid--three-col">
                <?php  while( have_rows('annual_reports') ) : the_row();
                  $paper_pdf = get_sub_field('paper_pdf');
                  $report_title = get_sub_field('report_title');
                  $report_thumb = get_sub_field('report_thumb');
                  $report_thumb_size_1 = "ann_report";
                ?>
                 <div class="l-grid-item">
                   <div class="thumbnail">
                     <img draggable="false" aria-hidden="true"
                       src="<?php echo esc_url( $report_thumb['sizes'][ $report_thumb_size_1 ] ); ?>"
                       width="<?php echo esc_attr( $report_thumb['sizes'][ $report_thumb_size_1 . '-width' ] ); ?>"
                       height="<?php echo esc_attr( $report_thumb['sizes'][ $report_thumb_size_1 . '-height' ] ); ?>"
                       alt="">
                   </div>
                   <div class="card-body">
                      <div class="card-header">
                        <h3 class="entry-title"><?php echo $report_title; ?></h3>
                      </div>

                      <div class="card-footer">
                        <?php if ( $paper_pdf ) :  ?>
                         <a class="button pdf--button"
                           href="<?php echo esc_url( $paper_pdf['url'] ); ?>"
                           target="_blank"> PDF Version
                         </a>
                       <?php endif; ?>
                      </div>
                   </div>
                 </div>
                <?php endwhile; ?>
              </div>
            <?php endif; ?>
          </div>
          <div class="l-aside-col">
              <?php get_sidebar(); ?>
            </div>
        </div>
      </div>
    </div>
  </section>

<?php get_footer(); ?>
