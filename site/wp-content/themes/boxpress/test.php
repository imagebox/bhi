<?php
/**
 * The template for displaying archive pages.
 *
 * Template Name: Team
 *
 * @package boxpress
 */


get_header(); ?>

<?php require_once('template-parts/banners/banner--team.php'); ?>

<section class="section about-grid">
  <div class="wrap">
    <div class="l-sidebar">
      <div class="l-aside-col">
        <?php get_sidebar('about'); ?>
    </div>
    <div class="l-main-col">



            <?php
              $team_query_args = array(
                'post_type' => 'team',
                'posts_per_page' => -1,
              );
              $team_query = new $wp_query( $team_query_args );
            ?>

          <?php if ( $team_query->have_posts() ) : ?>
            <section class="section staff-section">
              <div class="wrap">
                <div class="l-grid l-grid--three-col">
                  <?php while ( $team_query->have_posts() ) : $team_query->the_post(); ?>
                    <div class="l-grid-item">

                      <?php
                      $team_title = get_field('team_title');
                      ?>

                      <div class="card-shadow">
                        <a class="open-bio" href="#popup-<?php echo get_the_ID(); ?>">
                        <div class="card-body">
                          <div class="card-header">
                            <span><?php the_field('press_text'); ?></span>
                            <h3><?php the_title(); ?></h3>
                            <span class="board-title"><?php echo $team_title; ?></span>
                            <?php the_excerpt(); ?>
                          </div>
                          <div class="card-footer">
                            <div class="button button--text open-bio">
                              Learn More
                            </div>
                          </div>
                        </div>
                        </a>
                      </div>
                    </div>
                    <div id="popup-<?php echo get_the_ID(); ?>" class="mfp-hide popup">
                      <div class="wrap modal-wrap">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button title="Close (Esc)" type="button" class="mfp-close">  <span class="vh"><?php _e('Close', 'boxpress'); ?></span>
                              <svg class="menu-icon-svg" width="20" height="20" focusable="false">
                                <use href="#close-icon"/>
                              </svg><span class="custom-close"></span></button>
                              <?php if  ( has_post_thumbnail() ) : ?>
                                <div class="thumbnail">
                                  <?php the_post_thumbnail('blog-thumb');?>
                                </div>
                              <?php endif; ?>
                              <div class="modal-content">
                                <h2><?php the_title(); ?></h2>
                                <?php the_content(); ?>
                              </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  <?php endwhile; ?>
                </div>

              </div>
            </section>
            <?php wp_reset_postdata(); ?>
          <?php endif; ?>
    </div>
  </div>
</div>
</section>


<?php get_footer(); ?>
