<?php
/**
 * The template for displaying all single posts.
 *
 * @package boxpress
 */

get_header(); ?>

  <section class="section blog-page">
    <div class="wrap">
      <div class="l-sidebar">
        <div class="l-main-col">

          <?php while ( have_posts() ) : the_post(); ?>

            <?php get_template_part( 'template-parts/content/content', 'paper' ); ?>

            <?php
              // Load Comment Block
              if ( comments_open() || get_comments_number() ) :
                comments_template();
              endif;
            ?>

          <?php endwhile; ?>

        </div>
        <div class="l-aside-col">

          <?php get_sidebar(); ?>

        </div>
      </div>
    </div>
  </section>

<?php get_footer(); ?>
