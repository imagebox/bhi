<?php
/**
 * Template Name: Guidline
 *
 * Page template to display the advanced page builder.
 *
 * @package BoxPress
 */

  $guidline_year  = get_query_var( 'guidline_year' );
  $guidline_type  = get_query_var( 'guidline_type' );




get_header(); ?>

<?php require_once('template-parts/banners/banner--guidelines.php'); ?>

  <?php
    $post_query = array(
      'post_type' => 'guidline',
      'posts_per_page' => -1,
      'meta_key' => 'date',
      'orderby' => 'meta_value',
      'order' => 'DESC',
    );

    // Query for Year
    if ( ! empty( $guidline_year )) {
      $post_query = array_merge_recursive( $post_query, array(
        'tax_query' => array(
          array(
            'taxonomy'  => 'guidline_year',
            'field'     => 'slug',
            'terms'     => $guidline_year,
          ),
        ),
      ));
    }

    $post_loop = new WP_Query( $post_query );
  ?>


  <?php
    $guidline_terms_args = array(
      'taxonomy'   => 'guidline_year',
      'hide_empty' => true,
    );
    $guidline_terms = get_terms( $guidline_terms_args );
  ?>

  <?php if ( $guidline_terms && ! is_wp_error( $guidline_terms )  ) : ?>
    <?php foreach ( $guidline_terms as $term ) : ?>
      <?php
       $year_term_slug = $term->slug;
       $year_term_name = $term->name;
      ?>

      <section class="resource-grid">
        <div class="wrap">
          <div class="l-sidebar">
            <div class="l-main-col">

              <?php
                $guidline_args = array(
                  'post_type' => 'guidline',
                  'posts_per_page' => -1,
                  'order'=>'DESC',
                  'tax_query' => array(
                    array(
                      'taxonomy' => 'guidline_year',
                      'field'    => 'slug',
                      'terms'    => $year_term_slug,
                    ),
                  ),
                );

                $guidline_query = new $wp_query( $guidline_args );
              ?>

              <header>
                <h2 class="paper-year"><?php echo $year_term_name; ?></h2>
              </header>
              <?php if ( $guidline_query->have_posts() ) : ?>
                <div class="l-grid l-grid--three-col">
                <?php while ( $guidline_query->have_posts() ) : $guidline_query->the_post(); ?>

              <div class="l-grid-item">
                <div class="card-shadow">
                    <a class="open-bio" href="#popup-<?php echo get_the_ID(); ?>">
                      <div class="card-body">
                        <header class="entry-header">
                          <h3 class="entry-title"><?php the_title(); ?></h3>
                        </header>

                        <div class="entry-content">
                          <div class="paper-body">
                            <div class="button button--text open-bio">
                             <span>Learn More</span>
                            </div>
                          </div>
                        </div>
                      </div>
                     </a>
                  </div>
                  </div>

<?php   $paper_author = get_field('paper_author');
  $paper_pdf = get_field('paper_pdf');
  $paper_content = get_field('paper_content');
 ?>

                  <div id="popup-<?php echo get_the_ID(); ?>" class="mfp-hide popup">
                    <div class="wrap modal-wrap">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button title="Close (Esc)" type="button" class="mfp-close">  <span class="vh"><?php _e('Close', 'boxpress'); ?></span>
                            <svg class="menu-icon-svg" width="20" height="20" focusable="false">
                              <use href="#close-icon"/>
                            </svg><span class="custom-close"></span></button>
                            <?php if ( $paper_author ) :  ?>
                              <?php echo $paper_author; ?>
                            <?php endif; ?>

                            <?php if ( $paper_content ) :  ?>
                              <?php echo $paper_content; ?>
                            <?php endif; ?>


                            <?php if ( $paper_pdf ) :  ?>
                             <a class="button pdf--button"
                               href="<?php echo esc_url( $paper_pdf['url'] ); ?>"
                               target="_blank"> PDF Version
                             </a>
                           <?php endif; ?>
                        </div>
                      </div>
                    </div>
                  </div>



                <?php endwhile; ?>
              </div>
              <?php endif; ?>
            </div>
            <div class="l-aside-col">
              <?php get_sidebar('resources'); ?>
            </div>
          </div>
        </div>
      </section>
    <?php endforeach; ?>
  <?php endif; ?>

  <!-- end office  -->

<?php get_footer(); ?>
