<?php
/**
 * Displays the page template
 *
 * @package boxpress
 */

$child_pages_list = query_for_child_page_list();

?>
<?php get_header(); ?>


  <section class="fullwidth-column section">
    <div class="wrap <?php if ( ! $child_pages_list ) { echo 'wrap--limited'; } ?>">

      <div class="<?php if ( $child_pages_list ) { echo 'l-sidebar'; } ?>">
      <div class="l-sidebar">
        <div class="l-main-col">

          <?php while ( have_posts() ) : the_post(); ?>
            <?php get_template_part( 'template-parts/content/content', 'page' ); ?>
          <?php endwhile; ?>

          <div class="back-top back-top--article vh">
            <a href="#main"><?php _e('Back to Top', 'boxpress'); ?></a>
          </div>
        </div>


        <?php
        /**
         * Displays the page banner
         *
         * @package boxpress
         */

        $banner_title     = get_the_title();
        $banner_image_url = '';
        $default_banner   = get_field( 'default_banner_image', 'option' );

        if ( $default_banner ) {
          $banner_image_url = $default_banner['url'];
        }

        // Set top page title and banner image
        if ( 0 !== $post->post_parent ) {
          $post_ancestors = get_post_ancestors( $post->ID );
          $post_id = end( $post_ancestors );
          $banner_title = get_the_title( $post_id );
          $top_featured_image = get_the_post_thumbnail_url( $post_id );

          if ( ! empty( $top_featured_image )) {
            $banner_image_url = $top_featured_image;
          }
        }

        if ( has_post_thumbnail() ) {
          $banner_image_url = get_the_post_thumbnail_url( get_the_ID() );
        }
        ?>

                <div class="l-aside-col">
                  <?php if ( $child_pages_list ) : ?>
                      <?php get_sidebar(); ?>
                  <?php endif; ?>
                </div>
      </div>
    </div>
    </div>
  </section>

<?php get_footer(); ?>
