<?php
/**
 * The template for displaying archive pages.
 *
 * Template Name: Events
 *
 * @package boxpress
 */


get_header(); ?>

<?php require_once('template-parts/banners/banner--events.php'); ?>

<section class="section about-grid">
  <div class="wrap">
    <div class="l-sidebar">
      <div class="l-aside-col">
        <?php get_sidebar('resources'); ?>
    </div>
    <div class="l-main-col">



            <?php
              $events_query_args = array(
                'post_type' => 'events',
                'posts_per_page' => -1,
              );
              $events_query = new $wp_query( $events_query_args );
            ?>

          <?php if ( $events_query->have_posts() ) : ?>
            <section class="section staff-section  resource-grid">
              <div class="wrap">
                <div class="l-grid l-grid--three-col">
                  <?php while ( $events_query->have_posts() ) : $events_query->the_post(); ?>
                    <div class="l-grid-item">

                      <?php
                      $team_title = get_field('team_title');
                      ?>

                      <div class="card-shadow">
                        <a class="open-bio" href="#popup-<?php echo get_the_ID(); ?>">
                        <div class="thumbnail">
                          <?php the_post_thumbnail('blog-thumb');?>
                        </div>
                        <div class="card-body">
                          <div class="card--body">
                            <span><?php the_field('event_date'); ?></span>
                            <h3><?php the_title(); ?></h3>
                            <?php the_excerpt(); ?>
                          </div>
                          <div class="card-footer">
                            <div class="button button--text open-bio">
                              Learn More
                            </div>
                          </div>
                        </div>
                        </a>
                      </div>
                    </div>
                    <div id="popup-<?php echo get_the_ID(); ?>" class="mfp-hide popup">
                      <div class="wrap modal-wrap">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button title="Close (Esc)" type="button" class="mfp-close">  <span class="vh"><?php _e('Close', 'boxpress'); ?></span>
                              <svg class="menu-icon-svg" width="20" height="20" focusable="false">
                                <use href="#close-icon"/>
                              </svg><span class="custom-close"></span></button>
                              <?php if  ( has_post_thumbnail() ) : ?>
                                <div class="thumbnail">
                                  <?php the_post_thumbnail('blog-thumb');?>
                                </div>
                              <?php endif; ?>
                              <div class="modal-content">
                                <div class="header">
                                  <h2><?php the_title(); ?></h2>
                                  <?php the_field('event_text'); ?>
                                </div>
                                <footer class="entry-footer">
                                  <?php include( get_template_directory() . '/template-parts/social-share.php'); ?>
                                </footer>
                              </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  <?php endwhile; ?>
                </div>
              </div>
            </section>
            <?php wp_reset_postdata(); ?>
          <?php endif; ?>

    </div>
  </div>
</div>
</section>


<?php get_footer(); ?>
